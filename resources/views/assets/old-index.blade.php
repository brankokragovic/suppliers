<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
</head>

<body>
<section id="ds-section-search">
        <div class="container ds-container-search">
            <div class="dropdown-header-content">
            <div id="options" class="dropdown">
        <div class="title">
            Select an option
        </div>
        <ul class="sub_menu">
            <li data-submenu="open">Open only</li>
            <li data-submenu="closed">Closed only</li>
            <li data-submenu="all">Show all</li>
        </ul>
    </div>
            </div>
      
            <div class="ds-wrap-search">
                <input type="search" id="ds-search-products" placeholder="Search..." />
                <img src="./images/magnifying-glass.svg" class="ds-search-icons" alt="Search Icons" />
            </div>
           
            </div>
            
            <div class="ds-boolean">
            <input type="checkbox" name="checkbox" data-value="" id="ds-chbox-bollean" checked>
            <label for="ds-chbox-bollean">TEXT HERE</label>
        </div>

            <div class="ds-dropdown-wrap">
                <div class="container ds-container-dropdown">
                    <div class="ds-title-dropdown"><span></span></div>
                    <div class="ds-row-dropdown">
                        <div id="ds-filter-output">

                        </div>
                        <!--//.ds-filter-output-->
                    </div>
                    <!--//.ds-row-dropdown-->
                </div>
                <!--//.ds-container-dropdown-->
            </div>
            <!--//.ds-dropdown-wrap-->

        </div>
</section>
<section id="ds-first-data">
            <div class="ds-dropdown-wrap">
                <div class="container ds-container-dropdown">
                    <div class="ds-title-first-data-dropdown"><span></span></div>
                    <div class="ds-row-dropdown">
                        <div id="ds-first-filter-output">
                        </div>
                    </div>
                </div>
            </div>
            <!--//.ds-dropdown-wrap-->
   

</section>

<form id="ds-form-details" >
    <section id="detailed-informations">
        <div class="container ds-container-header">
            <div class="ds-header"></div>
        </div>
        <div class="container ds-container-details"></div>

    <div class="ds-container">
        <div  class="ds-last-part"> </div>
        <!-- M O D A L -->
        <div class="ds-modal-wrapper">
            <div class="ds-modal">
                <div class="ds-head">
                    <a class="ds-btn-close trigger" href="javascript:;"></a>
                </div>
                <div class="ds-content">
                    <p>
                        The ship date will be copied to approved Date.

                    </p>
                </div>
                <div class="ds-modal-footer">
                    <div id="ds-form-modal">
                        <div class="ds-modal-btn-box">
                            <button class="ds-modal-btn-yes open-popup" id="eventBtn">YES</button>
                            <button class="ds-modal-btn-cancel">NO</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- P O P U P -->
        <div class="ds-popup-wrapper">
            <div class="ds-popup">
                <div class="ds-head">
                    <a class="ds-btn-close close-popup" href="javascript:;"></a>
                </div>
                <div class="ds-content">
                    <p>
                        Your approval of order was sent to the customer.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!--//ds-last-part-->
    </section>

</form>
<!--FORM-->







<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>

</body>

</html>


