<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Document</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
</head>

<body>
<section class="page-header-logo">
    <h3>פורטל ספקים - אינטר מערכות אלקטרומכניות בע"מ</h3>
    <img class="logo-image" src="\images\INTER_E_S.JPG/">
</section>    
<section id="ds-section-search">
        <div class="container ds-container-search">
            <div class="dropdown-header-content">
            <div id="options" class="dropdown">
        <div class="title"> פתוחים בלבד</div>
        <ul class="sub_menu">
            <li class="sub-menu-choice" data-submenu="open">פתוחים בלבד</li>
            <li class="sub-menu-choice" data-submenu="closed">סגורים בלבד</li>
            <li class= "sub-menu-choice" data-submenu="all">הצג הכל</li>
        </ul>
    </div>
            </div>
      
            <div class="ds-wrap-search">
                <input type="search" id="ds-search-products" placeholder="חיפוש" />
                <img src="./images/magnifying-glass.svg" class="ds-search-icons" id='magnifier-icon' alt="Search Icons" />
            </div>
           
            </div>
            
            <div class="ds-boolean">
            
        
        </div>

            <div class="ds-dropdown-wrap">
                <div class="container ds-container-dropdown">
                    <div class="ds-title-dropdown"><span></span></div>
                    <div class="ds-row-dropdown">
                        <div id="ds-filter-output">

                        </div>
                        <!--//.ds-filter-output-->
                    </div>
                    <!--//.ds-row-dropdown-->
                </div>
                <!--//.ds-container-dropdown-->
            </div>
            <!--//.ds-dropdown-wrap-->

        </div>
</section>
<section id="ds-first-data" class="ds-first-data">
            <div class="ds-dropdown-wrap">
                <div class="container ds-container-dropdown">
                    <div class="ds-title-first-data-dropdown"><span></span></div>
                    <div class="ds-row-dropdown">
                        <div id="ds-first-filter-output">
                        </div>
                    </div>
                </div>
            </div>
            <!--//.ds-dropdown-wrap-->
</section>
<div id="ds-form-details" >
    <section id="detailed-informations" class="detailed-informations">
        <div class="container ds-container-header">
            <div class="ds-header">
                <div class="ds-project">
                    <p class="ds-title">פרויקט</p> 
                    <p>
                        <span class="ds-project-name" data-name="PrjName"></span>
                        <span class="ds-project-code" data-name="Project"></span>
                    </p>
                    <p>
                        <span class="ds-suppliers-code" data-name="CardCode"></span>
                        <span class="ds-suppliers-name" data-name="CardName"></span>
                    </p>
                </div>
                <div class="ds-wrap-2 clearfix">
                    <div class="ds-right ds-general-information-table-head-3">
                    <table>
                        <thead>
                            <tr>
                                <th>איש קשר</th>
                                <th>תאריך אספקה</th>
                                <th>תאריך</th>
                                <th>הזמנה מספר</th>
                            </tr>
                        </thead>
                    </table>
                    </div>
                    <div class="ds-right ds-general-information-table-body-3">
                        <table class="ds-third-table-body">
                            <tbody>
                                <tr>
                                    <td data-name="SuppliersContact"></td>
                                    <td data-name="DocDueDate"></td>
                                    <td data-name="DocDate"></td>
                                    <td data-name="DocNum" class="ds-doc-num"></td>
                                    <td data-name="Address" class="hidden-fields"></td>
                                    <td data-name="DocCur" class="hidden-fields"></td>
                                    <td data-name="DocTotalFC" class="hidden-fields"></td>
                                    <td data-name="JrnlMemo" class="hidden-fields"></td>
                                    <td data-name="DocRate" class="hidden-fields"></td>
                                    <td data-name="DocEntry" class="hidden-fields"></td>
                                    <td data-name="OPVersion" class="hidden-fields"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="ds-wrap-3 clearfix">
                    <div class="ds-right ds-general-information-table-head-3">
                        <table>
                            <thead>
                                <tr>
                                    <th>תנאי תשלום</th>
                                    <th>מע"מ</th>
                                    <th>סה"כ לפני מע"מ</th>
                                    <th>סה"כ כולל מע"מ</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="ds-right ds-general-information-table-body-3">
                        <table class="ds-third-table-body">
                            <tbody>
                                <tr>
                                    <td data-name="PaymentTerms"></td>
                                    <td data-name="VatSum"></td>
                                    <td data-name="SumBeforVat"></td>
                                    <td data-name="DocTotal"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="ds-wrap-2 clearfix">
                    <div class="ds-right ds-general-information-table-head-3 ">
                        <table class="small-table">
                            <thead>
                                <tr>
                                    <th>גרסאות קודמות</th>
                                    <th>סיבת שינוי</th>
                                    <th>גרסא </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="ds-right ds-general-information-table-body-3">
                        <table class="ds-third-table-body small-table">
                            <tbody>
                                <tr>
                                    <td>
                                        <select class="version-select">
                                        </select>
                                    </td>
                                    <td data-name="OPVersion"></td>
                                    <td data-name="POVersion">
                                        
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>          
            <!-- THIS IS DS HEADER END -->
            </div>
        </div>
        <div class="container ds-container-details">
            <div class="tbl-header">
	            <table cellpadding="0" cellspacing="0" border="0"> 
		            <thead>
			            <tr>
				            <th class="ds-suppliers-comments">הערות ספק לשורה</th>
                            <th class="ds-total-line">סה"כ לשורה</th>
                            <th class="ds-supp-price">מחיר מאושר</th>
				            <th class="ds-after-discount">אחרי הנחה </th>
				            
                            
                            <th class="ds-discount">% הנחה</th>
                            
                            <th class="ds-price">מחיר</th>
				            <th class="ds-supp-qty">כמות מאושרת</th>
				            <th class="da-qty">כמות</th> 
				            <th class="ds-remarks">הערות</th>
				            <th class="ds-approveddate">ת אספקה מאושר</th>
				            <th class="ds-shipdate">תאריך אספקה</th>
				            <th class="ds-po-version">מק"ט ספק</th>
				            <th class="ds-po-version">מק"ט יצרן</th>
				            <th class="ds-item-description">תיאור פריט</th>
				            <th class="ds-item-code">פריט</th> 
				            <th class="ds-serial-number">#</th>
			            </tr> 
		            </thead> 
	            </table> 
            </div>
            <div class="tbl-content">
	            <table cellpadding="0" cellspacing="0" border="0">
		            <tbody></tbody>
	            </table>
            </div>		
            <!-- END OF CONTAINER DETAILS -->
        </div>

    <div class="ds-container last-bottom-container">
        
        <div  class="ds-last-part">
            <div class="ds-form-content">
	            <div class="ds-total-count-wrapper">
		            <div class="ds-total-count-item ds-total-line-price">
			            <p data-name="PaymentTerms"></p>
			            <span >תנאי תשלום</span>
		            </div>
		            <div class="ds-total-count-item ds-total-line-price">
			            <p data-name="SumBeforVat"></p>
			            <span>סה"כ לפני מע"מ</span>
		            </div>
		            <div class="ds-total-count-item ds-total-line-price">
			            <p data-name="VatSum"></p>
			            <span>מע"מ</span>
		            </div>
		            <div class="ds-total-count-item ds-total-line-price">
			            <p data-name="DocTotal"></p>
			            <span>סה"כ כולל מע"מ</span>
		            </div>
	            </div>
	            <textarea data-name="Comments" id="" placeholder="הערות כלליות"> </textarea>
            </div>
            <div class="ds-btn-box">
	            <div class="form-group">
                <button type="button" class="ds-btn-submit trigger">אשר הזמנה</button>
                <button type="button" class="submit-button-reject">דחה הזמנה</button>
                <button type="button" class="submit-button-clear-all">נקה הכל</button>
                    <div class="ds-approve-date">
                        <span>אשר תאריכים </span>
			            <input type="checkbox" name="approve-date" id="approved-date">
                        <label class="check-label-dates" for="approve-date"></label>
                    </div>
                    <div class="approve-all">
                        <span>אשר כמויות ומחירים</span>
                        <input type="checkbox" name="approve-all" id="approved-all">
                        <label class="check-label-all" for="approve-all"></label>
                    </div>
                
            </div>
           <!-- LAST PART END IS HERE -->
        </div>
        
        <!-- M O D A L -->
        <!-- P O P U P -->
        
    </div>
    <div class="warning-modal">
        <p class="main-warning-text"></p>
            <div class="warning-modal-reject-buttons">
                <input type="text" class="reject-reason-input">
                <div class="inner-buttons-container">
                    <button id="reject-yes" class="submit-green disabled-button">שלח ביטול</button>
                    <button id="reject-no" class="submit-red">חזור להזמנה</button>
                </div>
            </div>
            <div class="warning-modal-buttons approve-delivery-days">
                <div class="warning-modal-buttons-container">
                    <button  class="warning-modal-button submit-green button-approve-dates-warning">כן</button>
                    <button  class="warning-modal-button submit-red">לא</button>
                    <div class="warning-modal-check-mark-container">
                        <div class="checkmark-first-part"></div>
                        
                    </div>
                </div>
                <span class="warning-modal-text-inner">אשר תאריכי אספקה</span>
            </div>
            <div class="warning-modal-buttons approve-quantities">
                <div class="warning-modal-buttons-container">
                    <button  class="warning-modal-button submit-green button-approve-quantities-warning">כן</button>
                    <button  class="warning-modal-button submit-red">לא</button>
                    <div class="warning-modal-check-mark-container">
                        <div class="checkmark-first-part"></div>
                        
                    </div>
                </div>
                <span class="warning-modal-text-inner">אשר כמויות</span>
            </div>
            <div class="warning-modal-buttons approve-ask-prices">
                <div class="warning-modal-buttons-container ">
                    <button  class="warning-modal-button submit-green button-approve-prices-warning">כן</button>
                    <button  class="warning-modal-button submit-red">לא</button>
                    <div class="warning-modal-check-mark-container">
                        <div class="checkmark-first-part"></div>
                        
                    </div>
                </div>
                <span class="warning-modal-text-inner">אשר מחירים</span>
            </div>
            <div class="warning-modal-buttons approve-all-warning">
                <div class="warning-modal-buttons-container ">
                    <button id="button-approve-all-warning" class="warning-modal-button submit-green button-approve-all-warning">כן</button>
                    <button  class="warning-modal-button submit-red">לא</button>
                    <div class="warning-modal-check-mark-container">
                        <div class="checkmark-first-part"></div>
                        
                    </div>
                </div>
                <span class="warning-modal-text-inner">אשר הכל</span>
            </div>
        </div>
    <!--//ds-last-part-->
    </section>

</div>
<!--FORM-->

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js'></script>
<script src="js/custom.js"></script>
</body>

</html>
