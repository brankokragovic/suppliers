<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





Route::get('/',function (){
    return view('assets.index');
});

Route::post('/data','SuppliersController@saerchData')->name('data');
Route::post('/certenData','SuppliersController@certenData')->name('certenData');
Route::post('/aditionalDetails','SuppliersController@additionalFilter')->name('aditionalDetails');
Route::post('/headerDetails','SuppliersController@headerDetails')->name('headerDetails');
Route::post('/submit','SuppliersController@submmit')->name('submit');
Route::post('/version','SuppliersController@getOldVersion')->name('getOldVersion');
Route::post('/reject','SuppliersController@reject')->name('reject');





Route::get('/checkNewData','ParseXMLController@checkNewData')->name('checkNewData');





