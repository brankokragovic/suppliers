<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Suppliers;

class SuppliersController extends Controller
{   
    public function saerchData(Request $request){

        $value = $request->name;
        $checkbox = $request->checkbox;
        $val = $request->value;
        $result =  DB::connection('sqlsrv')
        ->table('OPOR')->join('OPRJ' ,'OPRJ.PrjCode', '=',  'OPOR.Project','left outer')
        ->select('OPOR.DocNum as doc_number', 'OPOR.Project as project_code', 'OPOR.PrjName as project_name', 'OPOR.U_SupplierReplyDate as supplier')
        ->where('OPOR.Project','like',  $value .'%' )
        ->orWhere('OPRJ.PrjName', 'like',  $value .'%')
        ->orWhere('OPOR.DocNum', 'like', $value.'%')
        ->distinct('OPOR.Project')
        ->get();
        // AFTER WE UPDATE DATABSE WE CAN LIST BY SUPPLIER_REPLY_DATE ONLY LAST INSERT
        // OR LAST PO_VERSION          'OPOR.U_POVersion'
        // $element = $result->orderBy('U_SupplierReplyDate', 'DESC')->limit(1)->get();

        
        if($val == 'open'){
         $open = [];
         foreach($result as $res){
             if($res->supplier == null){
                 $open[] = $res; 
             }
         }
         
        return $open;
        }
      
        elseif($val == 'closed'){
          $closed  = [];
          foreach($result as $res){
              if($res->supplier != null){
                  $closed[] = $res;
                  
              }
          }
          
          return $closed;
        }
        elseif($val == 'all'){
            return $result;
        }

    }

    public function additionalFilter(Request $request){
        $keyword = $request->id;
        $val = $request->value;
       
        $db = DB::connection('sqlsrv')->table('OPOR')->select('DocEntry')
        ->where('Project', '=' ,$keyword)->get();
        $db =  json_decode($db,true);
        $b = [];

        foreach($db as $d =>$value){
            $b[] = $value['DocEntry'];
        }

        if(empty($b)){
            return ;
        }
      
        $result =  DB::connection('sqlsrv')->select("SELECT OPOR.DocNum, OPOR.Project, 
        OPRJ.PrjName, OPOR.DocEntry, OPOR.CardCode, 
        OPOR.CardName, OPOR.DocDate , OPOR.U_POVersion,U_SupplierReplyDate as supplier ,
        OPOR.DocTotal - OPOR.VatSum AS SumBeforVat   
        FROM         
        OPOR as OPOR LEFT OUTER JOIN
        OPRJ as OPRJ ON OPRJ.PrjCode = OPOR.Project
        WHERE OPOR.DocEntry IN(" . implode(',', $b) . ") 
        ORDER BY OPOR.DocDate DESC ");
       
        if($val == 'open'){
            $open = [];
            foreach($result as $res){
                if($res->supplier == null){
                    $open[] = $res; 
                }
            }
                return $open;
            }elseif($val == 'closed'){
                $closed  = [];
                foreach($result as $res){
                    if($res->supplier != null){
                        $closed[] = $res;
                    }
                }
                 return $closed;
            }elseif($val == 'all'){
                 return $result;
            }
               
    }

    public function headerDetails(Request $request){

        $keyword = $request->id;
        $val = $request->value;
        if($val == 'closed'){
            $result =  DB::connection('sqlsrv')->select("SELECT OPOR.DocNum, OPOR.DocDate, OPOR.DocDueDate, OPOR.Project, 
            OPOR.PrjName, OPOR.DocEntry, OPOR.CardCode, 
            OPOR.CardName, OPOR.DocTotal - OPOR.VatSum AS SumBeforVat, OPOR.VatSum, 
            OPOR.DocTotal, OPOR.Comments, OPOR.Address, OPOR.DocCur, OPOR.DocTotalFC , OPOR.JrnlMemo , OPOR.DocRate ,
            OPOR.U_POVersion AS POVersion, OPOR.U_OPVersionText AS OPVersion,
            OPOR.PaymentTerms AS 'PaymentTerms' ,OPOR.ContactName as 'SuppliersContact' 
            FROM         
            OPOR
            WHERE    
            (OPOR.DocEntry = $keyword ) AND (U_SupplierReplyDate IS NOT NULL)");
          $versions =   DB::connection('sqlsrv')->select("SELECT COUNT(OPOR.U_POVersion) as NumVerison
            FROM         
            OPOR
            WHERE    
            (OPOR.DocEntry = $keyword )");
           $version= '';
            foreach($versions as $v){
              
               $version = $v->NumVerison;
            }

            foreach($result as $res){        
               $res->version =$version;
            }
        
        return $result;

        }elseif($val == 'open'){

            $result =  DB::connection('sqlsrv')->select("SELECT OPOR.DocNum, OPOR.DocDate, OPOR.DocDueDate, OPOR.Project, 
            OPOR.PrjName, OPOR.DocEntry, OPOR.CardCode, 
            OPOR.CardName, OPOR.DocTotal - OPOR.VatSum AS SumBeforVat, OPOR.VatSum, 
            OPOR.DocTotal, OPOR.Comments, OPOR.Address, OPOR.DocCur, OPOR.DocTotalFC , OPOR.JrnlMemo , OPOR.DocRate ,
            OPOR.U_POVersion AS POVersion, OPOR.U_OPVersionText AS OPVersion,
            OPOR.PaymentTerms AS 'PaymentTerms' ,OPOR.ContactName as 'SuppliersContact'
            FROM         
            OPOR
            WHERE    
            (OPOR.DocEntry = $keyword ) AND (U_SupplierReplyDate IS  NULL)");

              $versions =   DB::connection('sqlsrv')->select("SELECT COUNT(OPOR.U_POVersion) as NumVerison
              FROM         
              OPOR
              WHERE    
              (OPOR.DocEntry = $keyword )");
           
             $version= '';
              foreach($versions as $v){
                
                 $version = $v->NumVerison;
              }
  
              foreach($result as $res){        
                 $res->version = $version;
              }
            return $result;

        }elseif($val == 'all'){
            $result =  DB::connection('sqlsrv')->select("SELECT OPOR.DocNum, OPOR.DocDate, OPOR.DocDueDate, OPOR.Project, 
            OPOR.PrjName, OPOR.DocEntry, OPOR.CardCode, 
            OPOR.CardName, OPOR.DocTotal - OPOR.VatSum AS SumBeforVat, OPOR.VatSum, 
            OPOR.DocTotal, OPOR.Comments, OPOR.Address, OPOR.DocCur, OPOR.DocTotalFC , OPOR.JrnlMemo , OPOR.DocRate ,
            OPOR.U_POVersion AS POVersion, OPOR.U_OPVersionText AS OPVersion,
            OPOR.PaymentTerms AS 'PaymentTerms' ,OPOR.ContactName as 'SuppliersContact'
            FROM         
            OPOR  
            WHERE    
           (OPOR.DocEntry = '$keyword' )");
            $versions =   DB::connection('sqlsrv')->select("SELECT COUNT(OPOR.U_POVersion) as NumVerison
            FROM         
            OPOR
            WHERE    
            (OPOR.DocEntry = $keyword )");
                 
           $version= '';
            foreach($versions as $v){
              
               $version = $v->NumVerison;
            }

            foreach($result as $res){        
               $res->version =$version;
            }

            return $result;
        }
    }

    public function certenData(Request $request){
      
       $keyword = $request->id;
       $val = $request->value;
     
       if($val == 'closed'){

        $result =  DB::connection('sqlsrv')
        ->select("SELECT T1.DocEntry AS LineDocEntry, T1.LineNum,
         T1.ItemCode, T1.Dscription, T1.Quantity, T1.VisOrder,
         T1.ShipDate, T1.U_ApprovedSupplyDate, 
         T1.FreeTxt AS LineComments, T1.SubCatNum, T1.VendorNum,
         T1.PriceBefDi, T1.DiscPrcnt, T1.Price, T1.LineTotal ,T0.U_SupplierReplyDate AS U_Supplier,
         T1.U_SuppFreeText,T1.U_SuppQuantity,T1.U_SuppPrice ,T1.Currency
        FROM OPOR AS T0 INNER JOIN
        POR1 AS T1 ON T0.DocEntry = T1.DocEntry INNER JOIN
        OPRJ AS T2 ON T0.Project = T2.PrjCode
        AND  (T1.DocEntry = $keyword ) AND (T0.U_SupplierReplyDate IS NOT NULL)");
       
        return $result;
       
       }elseif($val == 'open') {

        $result =  DB::connection('sqlsrv')
        ->select("SELECT T1.DocEntry AS LineDocEntry, T1.LineNum,
         T1.ItemCode, T1.Dscription, T1.Quantity, T1.VisOrder,
         T1.ShipDate, T1.U_ApprovedSupplyDate, 
         T1.FreeTxt AS LineComments, T1.SubCatNum, T1.VendorNum,
         T1.PriceBefDi, T1.DiscPrcnt, T1.Price, T1.LineTotal, T0.U_SupplierReplyDate AS U_Supplier,
         T1.U_SuppFreeText,T1.U_SuppQuantity,T1.U_SuppPrice,T1.Currency
        FROM OPOR AS T0 INNER JOIN
        POR1 AS T1 ON T0.DocEntry = T1.DocEntry INNER JOIN
        OPRJ AS T2 ON T0.Project = T2.PrjCode
        -- WHERE (T0.U_SuppliersApproval IS NULL) AND (T0.U_ApprovedKanian IS NOT NULL)
        AND  (T1.DocEntry = $keyword ) AND (T0.U_SupplierReplyDate IS NULL)");
     
        return $result;

        }elseif($val == 'all'){

        $result =  DB::connection('sqlsrv')
        ->select("SELECT T1.DocEntry AS LineDocEntry, T1.LineNum,
        T1.ItemCode, T1.Dscription, T1.Quantity, T1.VisOrder,
        T1.ShipDate, T1.U_ApprovedSupplyDate, 
        T1.FreeTxt AS LineComments, T1.SubCatNum, T1.VendorNum,
        T1.PriceBefDi, T1.DiscPrcnt, T1.Price, T1.LineTotal ,T0.U_SupplierReplyDate AS U_Supplier,
        T1.U_SuppFreeText,T1.U_SuppQuantity,T1.U_SuppPrice,T1.Currency
        FROM OPOR AS T0 INNER JOIN
        POR1 AS T1 ON T0.DocEntry = T1.DocEntry INNER JOIN
        OPRJ AS T2 ON T0.Project = T2.PrjCode
        WHERE  (T1.DocEntry = $keyword )");

        return $result;

       }
    }
     
    public function getOldVersion(Request $request){

       $POVersion = $request->U_POVersion;
       $DocEntry = $request->DocEntry;
       $result =  DB::connection('sqlsrv')
       ->select("SELECT T1.DocEntry AS LineDocEntry, T1.LineNum,
        T1.ItemCode, T1.Dscription, T1.Quantity, T1.VisOrder,
        T1.ShipDate, T1.U_ApprovedSupplyDate, 
        T1.FreeTxt AS LineComments, T1.SubCatNum, T1.VendorNum,
        T1.PriceBefDi, T1.DiscPrcnt, T1.Price, T1.LineTotal ,T0.U_SupplierReplyDate AS U_SupplierReplyDate,
        T1.U_SuppFreeText,T1.U_SuppQuantity,T1.U_SuppPrice,T1.Currency
        FROM OPOR AS T0 INNER JOIN
        POR1 AS T1 ON T0.DocEntry = T1.DocEntry 
        WHERE  (T1.DocEntry = $DocEntry )
       
        AND (T0.U_POVersion = $POVersion)");

       return $result;
    }

    public function reject(Request $request){
        //DocEntry\
       
        $input1 = array_filter($request->wholeOb);

        $PrjName = $input1[0]['PrjName'];
        $Project = $input1[0]['Project'];
        $CardCode = $input1[0]['CardCode'];
        $CardName = $input1[0]['CardName'];
        $SuppliersContact = $input1[0]['SuppliersContact'];
        $DocDueDate = date("Y-m-d", strtotime($input1[0]['DocDueDate']));
        $DocDate = date("Y-m-d", strtotime($input1[0]['DocDate']));
        $DocNum = $input1[0]['DocNum'];
        $Address = $input1[0]['Address'];
        $PaymentTerms = $input1[0]['PaymentTerms'];
        $SumBeforVat = $input1[0]['SumBeforVat'];
        $VatSum = $input1[0]['VatSum'];
        $DocTotal = $input1[0]['DocTotal'];
        $Comments = $input1[0]['Comments'];
        $POVersion = $input1[0]['POVersion'];
        $OPVersion = $input1[0]['OPVersion'];
        $DocEntry  = $input1[0]['DocEntry'];
        $DocCur = $input1[0]['DocCur'];
        $DocRate = $input1[0]['DocRate'];
        $DocTotalFC = $input1[0]['DocTotalFC'];
        $JrnlMemo = $input1[0]['JrnlMemo'];
        
       
        $keyword = $request->DocEntry;
        DB::table('OPOR')
                ->where('DocEntry', $keyword)
                ->update(['U_SupplierReplyDate' => NOW()]); 

                DB::table('OPOR_Response')
                ->where('DocEntry', $keyword)
                ->insert([
                'U_Reject_Reason' => $request->U_Reject_Reason
                ,'DocEntry' => $DocEntry
                ,'DocNum'      => $DocNum
                ,'U_POVersion'  => $POVersion
                ,'DocDate'     => $DocDate
                ,'DocDueDate'  => $DocDueDate
                ,'CardCode'    => $CardCode
                ,'CardName'    => $CardName
                ,'Address'     => $Address
                ,'VatSum'      => $VatSum
                ,'DocCur'      => doubleval( $DocCur)
                ,'DocRate'     =>doubleval( $DocRate)
                ,'DocTotal'    =>doubleval( $DocTotal)
                ,'DocTotalFC'  =>doubleval( $DocTotalFC)
                ,'Comments'    => $Comments
                ,'JrnlMemo'    => $JrnlMemo]);    
    } 
    public function submmit(Request $request){ 
      
         $input1 = array_filter($request->wholeOb);
         $input2 = array_filter( $request->multipleArr);
        
      // var_dump($input1);
     //  die();
        $PrjName = $input1[0]['PrjName'];
        $Project = $input1[0]['Project'];
        $CardCode = $input1[0]['CardCode'];
        $CardName = $input1[0]['CardName'];
        $SuppliersContact = $input1[0]['SuppliersContact'];
        $DocDueDate = date("Y-m-d", strtotime($input1[0]['DocDueDate']));
        $DocDate = date("Y-m-d", strtotime($input1[0]['DocDate']));
        $DocNum = $input1[0]['DocNum'];
        $Address = $input1[0]['Address'];
        $PaymentTerms = $input1[0]['PaymentTerms'];
        $SumBeforVat = $input1[0]['SumBeforVat'];
        $VatSum = $input1[0]['VatSum'];
        $DocTotal = $input1[0]['DocTotal'];
        $OPVersion = $input1[0]['OPVersion'];
        $Comments = $input1[0]['Comments'];
        $POVersion = $input1[0]['POVersion'];
        $OPVersion = $input1[0]['OPVersion'];
        $DocEntry  = $input1[0]['DocEntry'];
        $DocCur =  $input1[0]['DocCur'];
        $DocRate = $input1[0]['DocRate'];
        $DocTotalFC = $input1[0]['DocTotalFC'];
        $JrnlMemo = $input1[0]['JrnlMemo'];
       
   

         DB::connection('sqlsrv')->table('OPOR')
         ->where('DocEntry', '=',$DocEntry)
         ->where('DocNum' ,'=',$DocNum)->update([
          'DocEntry'         => $DocEntry
          ,'DocNum'          => $DocNum
          ,'U_POVersion'     => $POVersion
          ,'U_OPVersionText' => $OPVersion
          ,'DocDate'         =>date("Y-m-d", strtotime($DocDate)) 
          ,'DocDueDate'      => date("Y-m-d", strtotime($DocDueDate)) 
          ,'CardCode'        => $CardCode
          ,'CardName'        => $CardName
          ,'Address'         => $Address
          ,'VatSum'          => $VatSum
          ,'DocCur'          => doubleval( $DocCur)
          ,'DocRate'         =>doubleval( $DocRate)
          ,'DocTotal'        =>doubleval( $DocTotal)
          ,'DocTotalFC'      =>doubleval( $DocTotalFC)
          ,'Comments'        => $Comments
          ,'JrnlMemo'        => $JrnlMemo
          , 'U_SupplierReplyDate' => NOW()
        ]);

       DB::connection('sqlsrv')->table('OPOR_Response')
       ->where('DocEntry', '=',$DocEntry)
       ->where('DocNum' ,'=',$DocNum)->insert([
        'DocEntry'    => $DocEntry
       ,'DocNum'      => $DocNum
       ,'U_POVersion' => $POVersion
       ,'DocDate'     => $DocDate
       ,'DocDate'     => date("Y-m-d", strtotime($DocDate)) 
       ,'DocDueDate'  => date("Y-m-d", strtotime($DocDueDate)) 
       ,'CardName'    => $CardName
       ,'Address'     => $Address
       ,'VatSum'      => $VatSum
       ,'DocCur'      => doubleval( $DocCur)
       ,'DocRate'     => doubleval( $DocRate)
       ,'DocTotal'    => doubleval( $DocTotal)
       ,'DocTotalFC'  => doubleval( $DocTotalFC)
       ,'Comments'    => $Comments
       ,'JrnlMemo'    => $JrnlMemo
      ]);

        $data = [];
        foreach($input2 as $key => $value){
            
            $data[] = $value;
        }
        foreach($data as $info){  
            $U_SuppFreeText = $info['U_SuppFreeText'];
            $LineTotal = $info['LineTotal'];
            $Price = $info['Price'];
            $DiscPrcnt = $info['DiscPrcnt'];
            $PriceBefDi = $info['PriceBefDi'];
            $U_SuppPrice =  $info['U_SuppPrice'];
            $U_SuppQuantity = $info['U_SuppQuantity'];
            $Quantity = $info['Quantity'];
            $FreeText = $info['LineComments'];
            $U_SuppShipDate = $info['U_ApprovedSupplyDate'];
            $ShipDate = $info['ShipDate'];
            $SubbCatNum = $info['SubbCatNum'];
            $VendorNum = $info['VendorNum'];
            $Dscription = $info['Dscription'];
            $ItemCode = $info['ItemCode'];
            $SerialNumber = $info['SerialNumber'];
            $LineDocEntry = $info['LineDocEntry'];
            $LineNum = $info['LineNum'];
            $VisOrder = $info['VisOrder'];
        
          DB::connection('sqlsrv')->table('POR1')
            ->where('DocEntry', '=',$LineDocEntry)
            ->where('LineNum' ,'=',$LineNum)->update([
            'VendorNum'   => $VendorNum
            ,'U_SuppQuantity' => $U_SuppQuantity
            ,'U_ApprovedSupplyDate' =>date("Y-m-d", strtotime($U_SuppShipDate))  
            ,'U_SuppPrice' =>doubleval( $U_SuppPrice)
            ,'U_SuppFreeText' => $U_SuppFreeText
          //  ,  'U_SupplierReplyDate' => NOW() 
            , 'ItemCode' =>  $ItemCode
         // , 'SubbCatNum' =>  $SubbCatNum
            , 'VendorNum' =>  $VendorNum
            , 'Dscription' =>  $Dscription
            , 'ItemCode' =>  $ItemCode
            , 'FreeTxt' =>  $FreeText
            , 'Quantity' =>  $Quantity
            , 'ShipDate'  =>date("Y-m-d", strtotime($ShipDate))  
            , 'PriceBefDi' =>  $PriceBefDi
            , 'DiscPrcnt' =>  $DiscPrcnt
        //  , 'Currency' =>  $Currency
            , 'Price' =>  $Price
            , 'LineTotal' =>  $LineTotal
           
            ]);


            DB::connection('sqlsrv')->table('POR1_Response')
            ->where('DocEntry', '=',$LineDocEntry)
            ->where('LineNum' ,'=',$LineNum)->insert([
                    'DocEntry' => $LineDocEntry   
                    ,'LineNum' => $LineNum  
                    ,'VendorNum'   => $VendorNum
                //  ,'FreeTxt' => $FreeText
                    ,'Supplier_Quantity' => $U_SuppQuantity
                    ,'Supplier_ShipDate' => $U_SuppShipDate
                    ,'Supplier_Price' => $U_SuppPrice
                 // ,'U_SuppFreeText' => $U_SuppFreeText
                //  ,'U_SupplierReplyDate' => NOW() 
                    , 'ItemCode' =>  $ItemCode
                    , 'SubbCatNum' =>  $SubbCatNum
                    , 'VendorNum' =>  $VendorNum
                    , 'Dscription' =>  $Dscription
                    , 'ItemCode' =>  $ItemCode
                    , 'LineComments' =>  $FreeText
                    , 'Quantity' =>  $Quantity
                    , 'ShipDate'  =>date("Y-m-d", strtotime($ShipDate))  
                    , 'PriceBefDi' =>  $PriceBefDi
                    , 'DiscPrcnt' =>  $DiscPrcnt
               //   , 'Currency' =>  $Currency
                    , 'Price' =>  $Price
                    , 'LineTotal' =>  $LineTotal
                 // ,PO_Version
            ]);
        }
       
    }
}
