<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Suppliers;

class ParseXMLController extends Controller{
   
   public function checkNewData(){

      $newData = DB::connection('sqlsrv')->select
      ("SELECT TOP (100) PERCENT DocEntry, DocNum, ISNULL(U_POVersion, 0) AS 'Version'
      FROM Inter_E_S.dbo.OPOR AS T0
      WHERE (U_ApprovedKanian IS NOT NULL)
       AND (U_KanianApprovedSent IS NOT NULL)
       AND (DocStatus = 'O')
       AND (Project NOT IN ('777777'))
       AND (U_SuppliersPortal IS NULL)");
       $orders = [];
       foreach($newData as $data){
           //$data->DocEntry
             $orders[] = DB::connection('sqlsrv')->select("SELECT 
             OPOR.DocEntry	as 'DocEntry'	,
             OPOR.DocNum	as 'DocNumber'	,    
             OPOR.Canceled	,	
             CAST(YEAR(OPOR.DocDate) as NVARCHAR(4) ) + 
             RIGHT('0'+CAST(MONTH(OPOR.DocDate) as NVARCHAR(3)),2) + 
             RIGHT('0'+CAST( DAY(OPOR.DocDate) as NVARCHAR(3)),2) AS 'DocDate', 
             CAST(YEAR(OPOR.DocDueDate) as NVARCHAR(4) ) + 
             RIGHT('0'+CAST(MONTH(OPOR.DocDueDate) as NVARCHAR(3)),2) + 
             RIGHT('0'+CAST( DAY(OPOR.DocDueDate) as NVARCHAR(3)),2) AS 'DocDueDate', 
             OPOR.CardCode	as 'CardCode'	,	
             OPOR.CardName	as 'CardName'	,
             OPOR.Project	as 'Project'	,
             OPRJ.PrjName	,
             OPOR.DocTotal	AS 'DocTotal'	,
             OPOR.DocTotalSy	,
             OPOR.VatSum	AS 'VatSum'	,
             OPOR.DiscSum ,	
             OPOR.U_ApprovedKanian	,	
             OUSR.U_NAME	AS 'BuyerName'	,
             OPOR.U_FrameOrder	,
             OPOR.U_Equipment	AS 'U_Equipment'	,
             ISNULL( OPOR.U_POVersion, 0) AS	'U_POVersion'	,	
             Isnull(OPOR.U_OPVersionText,'') AS 'U_OPVersionText'	,
             OPOR.Comments	AS 'Comments'	,
             OCTG.PymntGroup	AS 'PaymentTerms'	,
             Case WHEN OPOR.U_Contact_Name <> Contacts.Name	THEN 
             OPOR.U_Contact_Name ELSE 
             Contacts.Name END 
             AS 'ContactName'	,	           
             Case WHEN OPOR.U_Contact_Tal1 <> Contacts.Cellolar	THEN 
             OPOR.U_Contact_Tal1 ELSE 
             Contacts.Cellolar END 
             AS 'ContactCellolar'	,
             isNULL(Contacts.ContactEmail, '')
             AS ContactEmail	,
             Contacts.CntctCode	,
             OPOR.DocCur	AS 'DocCur'	,	
             OUSR.U_NAME + '- '+ 
             RIGHT('0'+CAST( DAY(GetDate()) as NVARCHAR(3)),2) +'/'+ 
             RIGHT('0'+CAST(MONTH(GetDate()) as NVARCHAR(3)),2) + +'/'+	
             CAST(YEAR(GetDate()) as NVARCHAR(4) ) + ': ' + 
             OPOR.U_BuyersComments	AS 'BuyersComments'	,
             CPR0.Name	AS 'SuppliersContact'	,
             CPR0.Cellolar	AS 'SuppliersContactCell'	,
             OPOR.U_OrderType	AS 'U_OrderType'
                      
             FROM 
             Inter_E_S.dbo.OPOR	INNER JOIN 
             Inter_E_S.dbo.OCRD T0 ON OPOR.CardCode	= T0.CardCode INNER JOIN 
             Inter_E_S.dbo.OUSR ON OUSR.USERID	= OPOR.UserSign	INNER JOIN 
             Inter_E_S.dbo.OCTG ON OCTG.GroupNum	= OPOR.GroupNum	LEFT JOIN 
             (
             SELECT OCPR.CardCode, Name, ISNULL(Cellolar , IsNUll(tel1, ' ' ) ) AS Cellolar ,
             OCPR.E_MailL AS 'ContactEmail' ,
             CntctCode 
             FROM 
             Inter_E_S.dbo.OCPR INNER JOIN 
             Inter_E_S.dbo.OCRD ON OCRD.CardCode = OCPR.CardCode 
             WHERE OCRD.CntctPrsn = OCPR.Name 
             ) AS Contacts
             ON Contacts.CardCode = OPOR.Project LEFT JOIN 
             Inter_E_S.dbo.OCPR CPR0 ON CPR0.CntctCode = OPOR.CntctCode LEFT JOIN 
             Inter_E_S.dbo.OPRJ ON OPRJ.PrjCode = OPOR.Project 
             WHERE OPOR.DocEntry =$data->DocEntry");
       }  


       $docEntry = [];
       foreach($orders as $order){ 
          foreach($order as $value){
             $docEntry[]= $value->DocEntry;
            $check1 = DB::connection('sqlsrv')->table('OPOR')->where('DocEntry',  $value->DocEntry)->get();  
            if($check1->isEmpty()){   
            DB::table('OPOR')
            ->insert([ 'DocEntry' => $value->DocEntry,
            'DocNum' =>$value->DocNumber,
            'Canceled' => $value->Canceled,
            'DocDate' => $value->DocDate,
            'DocDueDate' => $value->DocDueDate,
            'CardCode' => $value->CardCode,
            'CardName' => $value->CardName,
            'Project' =>$value->Project,
            'DocTotalSy' => $value->DocTotalSy,
            'VatSum' => $value->VatSum,
            'DiscSum' => $value->DiscSum,
            'U_ApprovedKanian' => $value->U_ApprovedKanian,
            'DocTotal' => $value->DocTotal,
            'BuyerName' => $value->BuyerName,
            'U_FrameOrder' =>$value->U_FrameOrder,
            'U_Equipment' => $value->U_Equipment,
            'Comments' =>$value->Comments,
            'PaymentTerms' => $value->PaymentTerms,
            'ContactName' => $value->ContactName,
            'ContactCellolar' => $value->ContactCellolar,
            'DocCur' => $value->DocCur,
            'SuppliersContact' => $value->SuppliersContact,
            'U_OrderType' => $value->U_OrderType, 
            'U_POVersion' => $value->U_POVersion,
            'PrjName' => $value->PrjName,
            'U_OPVersionText' => $value->U_OPVersionText,
          //  'U_BuyersComments' =>$value->U_BuyersComments
            ]);
            }    
          }
       }
         $details = [];
        foreach($docEntry as $doc){
            $details[] = DB::connection('sqlsrv')
            ->select("SELECT 
            POR1.DocEntry	, 
            VisOrder + 1	as 'LineNumber'	,	
            POR1.ItemCode	as	'ItemCode'	,
            ISNULL(Oitm.BuyUnitMsr,'')	AS 'UOM'	,
            LineNum	as 'LineNum'	,
            ISNULL(SubCatNum, '') AS 'SubCatNum'	,
            Quantity	AS 'Quantity'	,
            OpenQty	AS 'OpenQty'	,
            PriceBefDi AS 'PriceBefDi'	,
            POR1.DiscPrcnt AS 'DiscPrcnt'	,
            Price	AS 'Price'	,	
            Currency	AS 'Currency'	,
            CASE 
            WHEN Rate <> 0 
            THEN Price * Rate 
            ELse Price END AS PriceNIS, 
            CASE 
            WHEN Rate <> 0 
            THEN LineTotal * Rate 
            ELse LineTotal END AS LineTotalNIS, 
            LineTotal	AS 'LineTotal'	,	 
            Dscription as	'Dscription'	,
            Isnull(FreeTxt,' ') as	'Remarks'	,
            IsNull(VendorNum,' ')	AS 'VendorNum'	,
            ShipDate	AS 'ShipDate'	,
            ISNULL(U_ApprovedSupplyDate,' ')	AS 'U_ApprovedSupplyDate'	,
            ISNULL( OPOR.U_POVersion, 0) AS  'U_POVersion'
            FROM 
            Inter_E_S.dbo.OPOR INNER JOIN 
            Inter_E_S.dbo.POR1 ON POR1.DocEntry = OPOR.DocEntry LEFT JOIN 
            Inter_E_S.dbo.OITM ON OITM.ItemCode = POR1.ItemCode	
            WHERE 
            POR1.DocEntry = $doc");
        }

        $versions = [];
        foreach($details as $detail){
           $versions[] = $detail;
        }

        foreach($versions as $values){
           foreach($values as $value){
         DB::table('POR1')
         ->insert([
             'DocEntry' => $value->DocEntry,     
             'ItemCode' => $value->ItemCode,
             'UOM' =>  $value->UOM,
             'LineNum' =>  $value->LineNum,
             'SubCatNum' => $value->SubCatNum,
             'Quantity' =>  $value->Quantity,
             'OpenQty' =>  $value->OpenQty,
             'PriceBefDi' =>  $value->PriceBefDi,
             'DiscPrcnt' => $value->DiscPrcnt,
             'Price' =>  $value->Price,
             'Currency' =>  $value->Currency,
             'PriceNIS' => $value->PriceNIS,
             'LineTotalNIS' => $value->LineTotalNIS,
             'LineTotal' => $value->LineTotal,
             'Dscription' =>  $value->Dscription,
             'FreeTxt' =>   $value->Remarks ,
             'VendorNum'  => $value->VendorNum  ,
         //    'LineAdditionalText'  => (!empty( $value->LineAdditionalText)) ?  $value->LineAdditionalText : '',  ,
             'ShipDate'  => $value->ShipDate,
             'U_ApprovedSupplyDate' =>  $value->U_ApprovedSupplyDate, 
             'U_POVersion'   => $value->U_POVersion             
         ]);   
           }
        }

      //foreach update this table !!!important 
      // this how we see how much data we have to update
      // for cron job on server
      //   UPDATE 
      //   Inter_E_S.dbo.OPOR 
      //   SET 
      //   U_SuppliersPortal = GetDate() 
      //   WHERE 
      //   DocEntry = 29844
        
   }
   
}