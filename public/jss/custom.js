
$(document).ready(function () {
   let $contentDetails = $('.ds-table-wrap-body').find('table');
   console.log($contentDetails);
   let $contentTable1 = $('.ds-first-table-body');
   $contentTable1.html(' <tr><td>Name</td> <td>30.9.2019</td> <td>10.9.2019</td>  <td>190442</td> </tr>')
   let $contentTable2 = $('.ds-second-table-body');
   $contentTable2.html('<tr> <td><input type="text" value=""  /></td>  <td>Lorem</td>  <td>3703</td></tr>')
   let $contentTable3 = $('.ds-third-table-body');
   $contentTable3.html('<tr> <td> 90+ ימים</td> <td> 183.000,00 nn</td> <td>n 26.000,00</td> <td>n 157.000,00</td> </tr>');


   // IF CHBOX CHEECKED SHOW MODAL ELSE SHOW TEXT "APPROVED DAT"
   $('.trigger').click(function () {
      if ($('#approved').is(':checked')) {
         $('.ds-modal-wrapper').toggleClass('open');

      } else {
         $('.form-group .ds-not-approved-date').html("Approved")
      }
   });
   // WHEN CHECK CHECKBOX REMOVE TEXT "APPROVED DATE"
   $('#approved').change(function () {
      if ($(this).is(':checked')) {
         $('.form-group .ds-not-approved-date').html("")
      };
   });

   // $('.open-popup').click(function () {
   //    $('.ds-popup-wrapper').toggleClass('open');
   //    $('.ds-modal-wrapper').removeClass('open');
   //    return false;
   // });

   $('.close-popup').click(function () {
      $('.ds-popup-wrapper').removeClass('open');
      $('.ds-modal-wrapper').removeClass('open');
      return false;
   });

   // search by keyword
   $('#ds-search-products').keyup(function () {
      var searchField = $(this).val();
      console.log(searchField)
      if (searchField === '') {
         $('#filter-records').html('');
         $(".ds-title-dropdown span").html("Output...");
         $('#ds-filter-output').html("");
         return;
      }
      $.ajax({
         type: "POST",
         url: "http://suppliers.devs/data",

          beforeSend: function () {

         },
         success: function (data) {

            try {
               console.log(data);
               var regex = new RegExp(searchField, "i");
               var output = '<ul class="ds-results-dropdown">';
               var count = 1;


               $.each(data, function (key, val) {
               //   console.log(val.C13, val.C43)
                  if ((val.project_code.search(regex) != -1) || (val.project_name.search(regex) != -1)) {

                     output += `<li class="ds-results" data-value="${val.project_name} ${val.project_code}" data-branko="${val.project_code}">${val.project_name} ${val.project_code}</li>`;
                     count++;
                  }

               });
               output += '</ul>';
               $('#ds-filter-output').html(output);

                $("#ds-filter-output li.ds-results").on('click', function() {
                    get_all_products( $(this).data('branko'))
                });
            } catch (e) {
               console.log(e);
            }


         }
      });
   });

   // put selected value in output
   $(".ds-title-dropdown span").html("Output...");
   $(document).on('click', '.ds-results', function (e) {
      var $this = $(this);
      var $selectedValue = $this.attr('data-value')
      if ($selectedValue) {
         $(".ds-title-dropdown span").html($selectedValue);
         $('.ds-results-dropdown').toggleClass("ds-toggle-dropdown");
      }

   });
   // toggleclass dropdown
   $('.ds-title-dropdown').on('click', function () {
      $('.ds-results-dropdown').toggleClass("ds-toggle-dropdown");
   })
   // add/remove active class
   $(document).on('click', 'li', function () {
      $('li').removeClass('ds-active');
      $(this).addClass('ds-active');
   });

   function get_all_products(all_data) {
      let $content1 = $('.ds-container-details');
      // $display.text("Loading data from JSON source...");

      $.ajax({
         type: "POST",
         url: "http://suppliers.devs/certenData",
          data: {'id' : all_data},
         beforeSend: function () {

         },

         success: function (data) {
            try {
        console.log(data)
               var output = '<div class="ds-table-wrap"> <div class="ds-table-wrap-head"> <table> <thead> <tr><th class="ds-suppliers-comments">Suppliers <br />Comments</th> <th class="ds-total-line">Total Line</th> <th class="ds-after-discount">After Disct</th> <th class="ds-discount">Disct %</th> <th class="ds-price">Price</th> <th class="ds-n3"></th> <th class="ds-n2"></th> <th class="da-qty">Qty</th> <th class="ds-remarks">Remarks</th> <th class="ds-item-description">Item Description</th> <th class="ds-approveddate">Approved <br />Date</th> <th class="ds-shipdate">ShipDate</th> <th class="ds-n1"></th> <th class="ds-item-code">Item Code</th> <th class="ds-serial-number">#</th> </tr> </thead> </table> </div> <div class="ds-table-wrap-body"> <table class="table-details"> <tbody>'
               for (let i in data) {
                  output += `<tr><td class="ds-suppliers-comments">  <textarea name="" id="" placeholder=""></textarea></td>  <td class="ds-total-line data-toggle="tooltip" data-placement="top" data-trigger="hover" title="${data[i].total_line}">${data[i].total_line}</td> <td class="ds-after-discount">${data[i].C2}</td> <td class="ds-discount">${data[i].disct}</td> <td class="ds-price">${data[i].price}</td> <td class="ds-n3"> <input type="text" value="" /> </td> <td class="ds-n2"> <input type="text" value="" /> </td> <td class="da-qty">${data[i].qty}</td> <td class="ds-remarks">${data[i].c2}</td> <td class="ds-item-description">${data[i].item_description}</td> <td class="ds-approveddate"> <input type="text" value="" name="approved-date" class="ds-users-approved-date"/> </td> <td class="ds-shipdate">${data[i].ship_date}</td> <td class="ds-n1"> <input type="text" value="" /> </td> <td class="ds-item-code">${data[i].item_code}</td> <td class="ds-serial-number">${data[i].C}</td> </tr>`
                //   output += `<tr><td>${data[i].item_code}</td></tr>`
               }
               output += " </tbody> </table> </div> </div>";
               $content1.html(output)

            } catch (e) {
               console.log(e)
            }
         },
      })//ajax

   }// function get_all_products
   $('#klikTest1').on('click', function () {
      get_all_products();
   })

   // let $approveddatevalues = document.getElementsByClassName('ds-users-approved-date');
   // let $approveddatearr = [];
   // let $str = ''

   $('.ds-modal-btn-cancel').on('click', function () {
      event.preventDefault();
      // PUCH VALUES OF INPUTS IN ARRAY
      // for (let i = 0; i < $approveddatevalues.length; i++) {
      //    $approveddatearr.push($approveddatevalues.item(i).value);
      //    $str = $str + ' , ' + $approveddatevalues.item(i).value;
      // }
      // IF VALUES OF INPUT EMPTY ADD CLASS
      $('.ds-users-approved-date').each(function () {
         let $this = $(this);
         let $values = $this.val();
         if ($values.length === 0) {
            $this.closest(".ds-approveddate").addClass('unfilled-approved-date');
         } else {
            $this.closest(".ds-approveddate").removeClass('unfilled-approved-date');
         }
      })
      console.log($approveddatearr);
      console.log($str);
   });

   $(".ds-modal-btn-yes").click(function () {
      showapproveddate();
   });
   function showapproveddate() {
      event.preventDefault();
      let $shipdatearray = [];
      $('td.ds-shipdate').each(function () {
         $shipdatearray.push($(this).text());
      });
      console.log($shipdatearray);

      let $el = $.map($shipdatearray, function(val, i) {
        return `<input value = ${val}/>`
      });
      console.log($el);
      $("td.ds-approveddate").html($el);


   }

});// READY

// });
