
jQuery(document).ready(function () {
    let dropdown_details = "open"
    console.log('svorcan initial', dropdown_details)
    // dropdown in header
    $('ul.sub_menu li').on('click', function(){
        let $this=$(this)
        dropdown_details = $this.data('submenu');
        console.log('svorcan',dropdown_details)
    })
    $(document).ready(function () {
        $("#options.dropdown").click(function () {
            $("ul.sub_menu").slideToggle(150);
        });
        $("#options.dropdown ul.sub_menu li").click(function () {
            var $this = $(this)
            $("div.title").text($this.text());

        });
    });

    // search by keyword
    $('#ds-search-products').keyup(function () {
        var searchField = $(this).val();
        if (searchField === '') {
            $('#filter-records').html('');
            $('#ds-section-search').find(".ds-title-dropdown span").html("Output...");

            $('#ds-filter-output').html("");
            return;
        }
        let $checkboxChange = $('#ds-chbox-bollean');
        if ($checkboxChange.is(":checked")) {
            $checkboxChange.val('true');
        } else {
            $checkboxChange.val('false');
        };
        let checkbox = $checkboxChange.val();
        


        // FIRST AJAX CALL IS HERE




        $.ajax({
            type: "POST",
            url: "/data",
            data: {
                checkbox: checkbox,
                searchField: searchField
            },
            beforeSend: function () {

            },
            success: function (data) {

                try {

                    var regex = new RegExp(searchField, "i");
                    var output = '<ul class="ds-results-dropdown">';
                    var count = 1;

                    $.each(data, function (key, val) {

                        if ((val.project_name.search(regex) != -1) || (val.project_code.search(regex) != -1) || (val.doc_number.search(regex) != -1)) {
                            output += `<li class="ds-results" data-value="${val.project_name} ${val.project_code}" data-branko="${val.project_code}"> ${val.project_name} ${val.project_code}</li>`;
                            count++;
                        }

                    });
                    output += '</ul>';
                    $('#ds-filter-output').html(output);

                    $("#ds-filter-output li.ds-results").on('click', function () {
                        let data = $(this).data('branko')
                        let value = dropdown_details
                        console.log('svorcan ajax click data', data, 'value', value)
                        $.ajax({
                            type: "POST",
                            url: "/aditionalDetails",
                            data: { 'id':data, 'value': value},
                
                            success: function (data) {
                                try {
                                    console.log('data ->>>>>',value);
                
                                     console.log("First output",data);
                                    $('#ds-first-filter-output').html("");
                                    let $firstData = '<ul class="ds-first-data-dropdown">';
                                    for (let i in data) {
                                        $firstData += ` <li class="ds-first-output" data-value="${data[i].CardCode}&nbsp;  &nbsp;${data[i].CardName} &nbsp;  &nbsp;${data[i].DocDate}" data-d="${data[i].DocEntry}">${data[i].CardCode} &nbsp;  &nbsp; ${data[i].CardName} &nbsp;  &nbsp;${data[i].DocDate}</li>`;
                                    }
                                    $firstData += '</ul>'
                                    $('#ds-first-filter-output').html($firstData);
                                } catch (e) {
                                    console.log(e)
                
                                }
                                $('.ds-first-output').on('click', function () {
                                    get_header_details($(this).data('d'))
                                    get_all_products($(this).data('d'))
                
                                })
                
                            },
                            error: function (error) {
                                console.log(error)
                            }
                
                        })



                    });


                } catch (e) {
                    console.log(e);
                }


            }
        });
    });

    // put selected value in output
    $(".ds-title-dropdown span").html("Output...");
    $('#ds-first-data').find(".ds-title-first-data-dropdown span").html("Select code");
    $(document).on('click', '.ds-results', function (e) {
        var $this = $(this);
        var $selectedValue = $this.attr('data-value')
        if ($selectedValue) {
            $(".ds-title-dropdown span").html($selectedValue);
            $('.ds-results-dropdown').toggleClass("ds-toggle-dropdown");
        }

        let $checkboxChange = $('#ds-chbox-bollean');
        if ($checkboxChange.is(":checked")) {
            $checkboxChange.val('true');
        } else {
            $checkboxChange.val('false');
        }
        console.log("Value of check chbox is", $checkboxChange.val())

    });
    $(document).on('click', '.ds-first-output', function () {
        var $this = $(this);
        var $selectedValue = $this.attr('data-value')
        if ($selectedValue) {
            $(".ds-title-first-data-dropdown span").html($selectedValue);
            $('.ds-first-data-dropdown').toggleClass("ds-toggle-dropdown");
        }
    });
    // toggleclass dropdown
    $('.ds-title-dropdown').on('click', function () {
        $('.ds-results-dropdown').toggleClass("ds-toggle-dropdown");
    });
    $('.ds-title-first-data-dropdown').on('click', function () {
        $('.ds-first-data-dropdown').toggleClass("ds-toggle-dropdown");
    });
    // add/remove active class
    $(document).on('click', 'li', function () {
        $('li').removeClass('ds-active');
        $(this).addClass('ds-active');
    });

    var searchField = $(this).val();
    // console.log(searchField)
    if (searchField === '') {
        $('#filter-records').html('');
        $('#ds-section-search').find(".ds-title-dropdown span").html("Output...");
        $('#ds-filter-output').html("");
        return;
    }
    
    function get_header_details(data) {
        let $header = $('.ds-header');
        let $form = $('.ds-last-part')
        $.ajax({
            type: "POST",
            url: "/headerDetails",
            data: { 'id': data, 'value': dropdown_details },

            beforeSend: function () {

            },
            success: function (data) {
                try {
                    console.log("header", data)
                    let $projectDetails = ""
                    let $formDetails = ""
                    for (let i in data) {
                        $projectDetails = `<div class="ds-project"> <p class="ds-title">Project</p> <p><span class="ds-project-name" data-name="PrjName">${data[i].PrjName}</span> &nbsp; <span class="ds-project-code" data-name="Project">${data[i].Project}</span> </p> <p><span class="ds-suppliers-code" data-name="CardCode">${data[i].CardCode}</span> &nbsp; <span class="ds-suppliers-name" data-name="CardName">${data[i].CardName}</span></p> </div> <div class="ds-wrap-2 clearfix"> <div class="ds-right ds-general-information-table-head-3"> <table> <thead> <tr> <th>Contact</th> <th>Delivery Date</th> <th>Order Date</th> <th>Order #</th> </tr> </thead> </table> </div> <div class="ds-right ds-general-information-table-body-3"> <table class="ds-third-table-body"> <tbody> <tr> <td data-name="SuppliersContact">${data[i].SuppliersContact}</td> <td data-name="DocDueDate">${data[i].DocDueDate}</td> <td data-name="DocDate">${data[i].DocDate}</td> <td data-name="DocNum" class="ds-doc-num">${data[i].DocNum}</td> <td data-name="Address" class="hidden-fields">${data[i].Address}</td> <td data-name="DocCur" class="hidden-fields">${data[i].DocCur}</td> <td data-name="DocTotalFC" class="hidden-fields">${data[i].DocTotalFC}</td> <td data-name="JrnlMemo" class="hidden-fields">${data[i].JrnlMemo}</td> <td data-name="DocRate" class="hidden-fields">${data[i].DocRate}</td> <td data-name="DocEntry" class="hidden-fields">${data[i].DocEntry}</td> <td data-name="OPVersion" class="hidden-fields">${data[i].OPVersion}</td> </tr> </tbody> </table> </div> </div> <div class="ds-wrap-3 clearfix"> <div class="ds-right ds-general-information-table-head-3"> <table> <thead> <tr> <th>Payment Terms</th> <th>Before Vat</th> <th>VAT</th> <th>Total Amount</th> </tr> </thead> </table> </div> <div class="ds-right ds-general-information-table-body-3"> <table class="ds-third-table-body"> <tbody> <tr> <td data-name="PaymentTerms">${data[i].PaymentTerms}</td> <td data-name="SumBeforVat">${data[i].SumBeforVat}</td> <td data-name="VatSum">${data[i].VatSum}</td> <td data-name="DocTotal">${data[i].DocTotal}</td> </tr> </tbody> </table> </div> </div> <div class="ds-wrap-2 clearfix"> <div class="ds-right ds-general-information-table-head-3 "> <table class="small-table"> <thead> <tr> <th>Change Reason</th> <th>Version #</th> </tr> </thead> </table> </div> <div class="ds-right ds-general-information-table-body-3"> <table class="ds-third-table-body small-table"> <tbody> <tr> <td data-name="ChangeReason">${data[i].P}</td> <td data-name="POVersion">${data[i].POVersion}</td> </tr> </tbody> </table> </div> </div>`
                        $formDetails = `<diov class="ds-form-content"> <div class="ds-total-count-wrapper"> <div class="ds-total-count-item ds-total-line-price"> <p>${data[i].PaymentTerms}</p> <span data-name="PaymentTerms"> Payment Terms</span> </div> <div class="ds-total-count-item ds-total-line-price"> <p data-name="SumBeforVat">${data[i].SumBeforVat}</p> <span>Before VAT </span> </div> <div class="ds-total-count-item ds-total-line-price"> <p data-name="VatSum">${data[i].VatSum}</p> <span>VAT</span> </div> <div class="ds-total-count-item ds-total-line-price"> <p data-name="DocTotal">${data[i].DocTotal}</p> <span>Total Amount</span> </div> </div> <textarea data-name="Comments" id="" placeholder="General Comments"></textarea> </div> <div class="ds-btn-box"> <div class="form-group"> <div class="ds-not-approved-date">Approve</div> <input type="checkbox" name="aprroved" id="approved"> <label for="approved"></label> </div> <button type="button" class="ds-btn-submit trigger">SUBMIT</button> </div>`
                    }
                    $header.html($projectDetails);
                    $form.html($formDetails);

                } catch (e) {
                    console.log(e)

                }

            }


        })//ajax foe get header details

    }
    function get_all_products(all_data) {
        let $content1 = $('.ds-container-details');



        // SECOND AJAX CALL IS HERE


        console.log('svorcan check all data second ajax', all_data, 'this is value dropdown', dropdown_details)
        $.ajax({
            type: "POST",
            url: "/certenData",
            data: { 'id': all_data, 'value': dropdown_details },
            beforeSend: function () {

            },

            success: function (data) {
                try {
                    console.log("Details table", data)
                    let n = 1;
                    let output = '<div class="tbl-header"> <table cellpadding="0" cellspacing="0" border="0"> <thead> <tr> <th class="ds-suppliers-comments">Suppliers Comments</th> <th class="ds-total-line">Total Line</th> <th class="ds-after-discount">After Disct</th> <th class="ds-discount">Disct %</th> <th class="ds-price">Price</th> <th class="ds-supp-price">Supp. Price</th> <th class="ds-supp-qty">Suppl. Qty</th> <th class="da-qty">Qty</th> <th class="ds-remarks">Remarks</th> <th class="ds-approveddate">Approved Date</th> <th class="ds-shipdate">ShipDate</th> <th class="ds-po-version">SubbCatNumber</th> <th class="ds-po-version">VendorNumber</th> <th class="ds-item-description">Item Description</th> <th class="ds-item-code">Item Code</th> <th class="ds-serial-number">#</th> </tr> </thead> </table> </div> <div class="tbl-content"> <table cellpadding="0" cellspacing="0" border="0"> <tbody>'
                    for (let i in data) {
                        output += ` <tr data-id=${n + 'dataid'} class='info-container-row'> <td class="ds-suppliers-comments"> <textarea data-multiple-name="U_SuppFreeText" id="" placeholder="Suppliers Comments"></textarea> </td> <td class="ds-total-line" data-multiple-name="LineTotal"> ${data[i].LineTotal}</td> <td class="ds-after-discount" data-multiple-name="Price">${data[i].Price}</td> <td class="ds-discount" data-multiple-name="DiscPrcnt">${data[i].DiscPrcnt}</td> <td class="ds-price" data-multiple-name="PriceBefDi">${data[i].PriceBefDi}</td> <td class="ds-supp-price"> <input type="number" value="" data-multiple-name="U_SuppPrice" /> </td> <td class="ds-supp-qty"> <input data-multiple-name="U_SuppQuentity" type="number" value="" /> </td> <td class="da-qty" data-multiple-name="Quantity">${data[i].Quantity}</td> <td class="ds-remarks" ><input type="text" value="${data[i].LineComments}" data-multiple-name="LineComments"/></td> <td class="ds-approveddate"> <input type="date" value="" data-multiple-name="U_SuppShipDate" class="ds-users-approved-date meeting"  /> </td> <td class="ds-shipdate" data-multiple-name="ShipDate">${data[i].ShipDate}</td> <td class="ds-po-version"> <input type="number" value="${data[i].SubCatNum}" data-multiple-name="SubbCatNum" /> </td> <td class="ds-po-version"> <input type="number" 
                            value="${data[i].VendorNum}" data-multiple-name="VendorNum" /> </td> <td class="ds-item-description" data-multiple-name="Dscription">${data[i].Dscription}</td> <td class="ds-item-code" data-multiple-name="ItemCode">${data[i].ItemCode}</td> <td class="ds-serial-number" data-multiple-name="SerialNumber">${n++}</td><td class="hidden-fields" data-multiple-name="LineDocEntry">${data[i].LineDocEntry}</td> <td class="hidden-fields" data-multiple-name="LineNum">${data[i].LineNum}</td> <td class="hidden-fields" data-multiple-name="VisOrder">${data[i].VisOrder}</td></tr>` //   output += `<tr><td>${data[i].item_code}</td></tr>`
                    }
                    output += " </tbody> </table> </div> ";
                    $content1.html(output)

                } catch (e) {
                    console.log(e)
                }
            },
        })//ajax

    }// function get_all_products

    //
    // IF CHECKBOX BOOLEAN CHECK



    // IF CHBOX CHEECKED SHOW MODAL ELSE SHOW TEXT "APPROVED DAT"

    $(document).on('click', '.trigger', function () {

        if ($('#approved').is(':checked')) {
            $('.ds-modal-wrapper').toggleClass('open');

        } else {
            $('.form-group .ds-not-approved-date').html("You must approved");
        }
    })

    // WHEN CHECK CHECKBOX REMOVE TEXT "APPROVED DATE"
    $(document).on('change', '#approved', function () {
        if ($(this).is(':checked')) {
            $('.form-group .ds-not-approved-date').html("");

        };
    })








});// READY

function dateToText() {

    $('.meeting').each(function () {
        var $inputDate = $(this);
        if ($inputDate.attr('type') === 'date') {
            $inputDate.attr('type', 'text')
            console.log("text")
        }
    })
}
$(document).on("click", ".ds-modal-btn-yes", function () {
    $('.ds-users-approved-date').each(function () {
        event.preventDefault();
        var $this = $(this);
        var $val = $this.val();

        console.log("Value of appDate", $val);
        dateToText();

        var shipdate = $.trim($this.parent().next().text()).slice(0, -1)
        $this.val($val === "" ? shipdate : $val);
        $('.ds-popup-wrapper').addClass('open');
        $('.ds-modal-wrapper').removeClass('open');
        $('.ds-approveddate').removeClass('unfilled-approved-date');
    })
});

$(document).on('click', '.trigger', function () {
    if ($('#approved').is(':checked')) {
        $('.ds-modal-wrapper').toggleClass('open');

    } else {
        $('.form-group .ds-not-approved-date').html("You must approved");
    }
});
$(document).on('click', '.ds-modal-btn-cancel', function () {
    event.preventDefault();

    // IF VALUES OF INPUT EMPTY ADD CLASS
    $('.ds-users-approved-date').each(function () {
        let $this = $(this);
        let $values = $this.val();
        if ($values.length === 0) {
            $this.closest(".ds-approveddate").addClass('unfilled-approved-date');
        } else {
            $this.closest(".ds-approveddate").removeClass('unfilled-approved-date');
        }
    });

});
$('.close-popup').click(function () {
    $('.ds-popup-wrapper').removeClass('open');
    $('.ds-modal-wrapper').removeClass('open');
    location.reload();
    return false;
});
// CLICK ON BTN IN MODAL FOR SUBMIT
function submit() {
    let nameSingle = $('[data-name]').attr('data-name');
    let allData = $("[data-name]");
    let multipleData = $('.info-container-row');
    let i;
    let uniqeObj = {};
    let multipleObj = {};
    for (i = 0; i < allData.length; i++) {
        let nameOfProperty = $(allData[i]).attr('data-name');
        let isInput = allData[i].tagName === 'INPUT' || allData[i].tagName === 'TEXTAREA' ? true : false;

        if (isInput) {
            valueOfProperty = $(allData[i]).val();
        } else {
            valueOfProperty = $(allData[i]).text();
        }
        // let valueOfProperty = $(allData[i]).text();
        uniqeObj[nameOfProperty] = valueOfProperty;
    }

    $('.info-container-row').each(function (el, le) {
        let allInputs = $(le).find('[data-multiple-name]');
        let currObjFirst = {};
        let i;

        for (i = 0; i < allInputs.length; i++) {
            let nameOfProperty = $(allInputs[i]).attr('data-multiple-name');
            let valueOfProperty;
            let isInput = allInputs[i].tagName === 'INPUT' || allInputs[i].tagName === 'TEXTAREA' ? true : false;

            if (isInput) {
                valueOfProperty = $(allInputs[i]).val();
            } else {
                valueOfProperty = $(allInputs[i]).text();
            }
            currObjFirst[nameOfProperty] = valueOfProperty;
        }
        multipleObj[el] = currObjFirst;
    });

    console.log('OVo je objekat', multipleObj, uniqeObj);

    $.ajax({
        type: "POST",
        url: "/submit",
        data: {
            'multipleArr': multipleObj,
            'wholeOb': uniqeObj
        },
        success: function (data) {
            try {
                console.log(data)

            } catch (e) {
                console.log(e)
            }
        },
        error: function (e) {
            console.log(e);
        }


    })//ajax
}
$(document).on('click', '#eventBtn', function () {
    submit();
});
