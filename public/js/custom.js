jQuery(document).ready(function () {

    //GLOBAL VARIABLES ARE HERE

    //default dropdown value
    let dropdown_details = "open";

    let initial_data_details;
    let first_output_details;
    let headerData;
    let certainData;
    let checkbox;
    let searchField;
    let selectedProject;

    let warningPricesApproved = false;
    let warningQuantitiesApproved = false;
    let warningDatesApproved = false;
    //GLOGAL VARIABLES END


    
    // CLICK EVENTS

    $('.submit-button-clear-all').click(function(e){
        e.preventDefault()
        
        warningDatesApproved = false;
        warningPricesApproved = false;
        warningQuantitiesApproved = false;

        $('.checkmark-first-part').removeClass('checkmark-active')
        $('.ds-users-approved-date').val('')
        $('.price-input').val('')
        $('.qty-input').val('')
        $('.ds-suppliers-comments textarea').val('')
        $('.warning-modal').removeClass('warning-modal-active')
        $('input').removeClass('valid-input')
        $('input').removeClass('invalid-input')
        $('.ds-approve-date .check-label-dates').removeClass('label-check-mark')
        $('.ds-approve-date span').removeClass('text-color-green')
        $('.approve-all .check-label-all').removeClass('label-check-mark')
        $('.approve-all span').removeClass('text-color-green')
        $('.price-input').addClass('invalid-input')
        $('.qty-input').addClass('invalid-input')
        $('.ds-users-approved-date').addClass('invalid-input')
        
        registerInputClick();
    });
    $('.button-approve-dates-warning').click(function(e){
        let parent = $(this).parent()
        parent.find('.checkmark-first-part').addClass('checkmark-active')
        
        copyDatesToFields();
        e.preventDefault()
        
        if(!$('.approve-delivery-days').is(':visible') && !$('.approve-quantities').is(':visible') && !$('.button-approve-prices-warning').is(':visible')) {
            $('.warning-modal').removeClass('warning-modal-active')
        }
    });
    $('.button-approve-quantities-warning').click(function(e){
        let parent = $(this).parent()
        parent.find('.checkmark-first-part').addClass('checkmark-active')
        
        e.preventDefault()
        copyQuantityToFields();
        
        if(!$('.approve-delivery-days').is(':visible') && !$('.approve-quantities').is(':visible') && !$('.button-approve-prices-warning').is(':visible')) {
            $('.warning-modal').removeClass('warning-modal-active')
        }
    });
    $('.button-approve-prices-warning').click(function(e){
        let parent = $(this).parent()
        parent.find('.checkmark-first-part').addClass('checkmark-active')

        e.preventDefault()
        copyPriceToFields();

        if(!$('.approve-delivery-days').is(':visible') && !$('.approve-quantities').is(':visible') && !$('.button-approve-prices-warning').is(':visible')) {
            $('.warning-modal').removeClass('warning-modal-active')
        }
    });
    $('#button-approve-all-warning').click(function(){

        if(!warningDatesApproved) {
            copyDatesToFields();
        }
        if(!warningPricesApproved) {
            copyPriceToFields();
        }
        if(!warningQuantitiesApproved) {
            copyQuantityToFields();
        }
        $('.warning-modal').removeClass('warning-modal-active')
    });
    $('.approve-all-warning .submit-red').click(function(){
        
        let prices = $('.price-input')
        let quantities = $('.qty-input')
        let dates = $('.ds-users-approved-date')
        
        resetFields(prices)
        resetFields(quantities)
        resetFields(dates)

        $('.checkmark-first-part').removeClass('checkmark-active')
    });
    $('.approve-ask-prices .submit-red').click(function(){
        $(this).parent().find('.checkmark-first-part').removeClass('checkmark-active')
        resetFields($('.price-input'))
    });
    $('.approve-quantities .submit-red').click(function(){
        $(this).parent().find('.checkmark-first-part').removeClass('checkmark-active')
        resetFields($('.qty-input')) 
    });

    $('.approve-delivery-days .submit-red').click(function(){
        $(this).parent().find('.checkmark-first-part').removeClass('checkmark-active')
        resetFields($('.ds-users-approved-date'))
    });

    $('#magnifier-icon').click(function(){
        ajaxDetails()
        $('#ds-section-search').find(".ds-title-dropdown span").html("פרויקט");
        $('.ds-first-data-dropdown').remove()
        $('.ds-title').css({'display':'none'})
        $(".ds-header").find("[data-name]").html('')
        $(".ds-container").find("[data-name]").html('')
        $(".ds-container").find("[data-name='Comments']").val('')
        $(".ds-wrap-2").css({"display":"none"});
        $(".ds-wrap-3").css({"display":"none"});
        $(".ds-project p").css({"display":"none"});
        $('.ds-project .ds-title').css({"display":"block"}).text('לא נמצאו נתונים')
        $('.ds-container-details').css({"display": "none"});
        $('.ds-container').css({'display': "none"});
        $(".detailed-informations").removeClass('detailed-informations-active')
        $(".ds-header").removeClass('ds-header-active')
        $('#ds-first-data').removeClass('ds-first-data-active')
        $('#ds-first-data').find(".ds-title-first-data-dropdown span").html("בחר הזמנה");
    }); 

    $('#reject-no').click(function(){
        
        $('.warning-modal').removeClass('warning-modal-active')
        setTimeout(function(){
            $('.main-warning-text').css({"display":"none", "color":"green"})
            $('.approve-delivery-days').removeClass('warning-modal-buttons-disable')
            $('.approve-quantities').removeClass('warning-modal-buttons-disable')
            $('.approve-ask-prices').removeClass('warning-modal-buttons-disable')
            $('.approve-all-warning').removeClass('warning-modal-buttons-disable')
        },300)
    });

    $('.check-label-dates').on('click',function(){
        copyDatesToFields($(this))
    });

    $('.check-label-all').on('click',function(){
        
        copyQuantityToFields ();
        copyPriceToFields ();
    
        $(this).toggleClass('label-check-mark')
        let text = $(this).parent().find('span');
        text.toggleClass('text-color-green')

        if(text.hasClass('text-color-green')) {
            text.text('כמויות ומחירים אושרו')
        }
        else {
            text.text('אשר כמויות ומחירים')
        }
    });

    $('ul.sub_menu li').on('click', function(){
        
        let $this=$(this)
        dropdown_details = $this.data('submenu');
            $('#ds-section-search').find(".ds-title-dropdown span").html("פרויקט");
            $('.ds-first-data-dropdown').remove()
            $('#ds-first-data').removeClass('ds-first-data-active')
            $(".ds-header").removeClass('ds-header-active')
            $('.ds-title').css({'display':'none'})
            $(".ds-header").find("[data-name]").html('')
            $(".ds-container").find("[data-name]").html('')
            $(".ds-container").find("[data-name='Comments']").val('')
            $(".ds-wrap-2").css({"display":"none"});
            $(".ds-wrap-3").css({"display":"none"});
            $(".ds-project p").css({"display":"none"});
            $('.ds-project .ds-title').css({"display":"block"}).text('לא נמצאו נתונים')
            $('.ds-container-details').css({"display": "none"});
            $('.ds-container').css({'display': "none"});
            ajaxDetails ()
        

        // if(selectedProject && selectedProject.length <= 1) {
        //     ajaxDetails ()
        // }
        
        
    }); 
    
    $("#options.dropdown").click(function () {
        $("ul.sub_menu").slideToggle(150);
    }); 

    $("#options.dropdown ul.sub_menu li").click(function () {
        
        var $this = $(this)
        $("div.title").text($this.text());
    });

    $(document).on('click', 'li.ds-results' ,function () {
        
        $(".ds-header").removeClass('ds-header-active')
        $('.ds-title').css({'display':'none'})
        $(".ds-header").find("[data-name]").html('')
        $(".ds-container").find("[data-name]").html('')
        $(".ds-container").find("[data-name='Comments']").val('')
        $(".ds-wrap-2").css({"display":"none"});
        $(".ds-wrap-3").css({"display":"none"});
        $(".ds-project p").css({"display":"none"});
        $('.ds-project .ds-title').css({"display":"block"}).text('לא נמצאו נתונים')
        $('.ds-container-details').css({"display": "none"});
        $('.ds-container').css({'display': "none"});

        let data = $(this).data('branko')
        let dropdown = dropdown_details
        initial_data_details = $(this).data('branko');
        ajaxAdditionalDetails (data,dropdown)
    });

    $(document).on('click', '.ds-results', function (e) {
        $(".ds-first-data").addClass('ds-first-data-active')
        var $this = $(this);
        selectedProject = $this.attr('data-value')
       
        if (selectedProject) {
            $(".ds-title-dropdown span").html(selectedProject);
            $('.ds-results-dropdown').toggleClass("ds-toggle-dropdown");
        }

        let $checkboxChange = $('#ds-chbox-bollean');
        if ($checkboxChange.is(":checked")) {
            $checkboxChange.val('true');
        } else {
            $checkboxChange.val('false');
        }  
    });

    $(document).on('click', '.ds-first-output', function () {
        $('.warning-modal').removeClass('warning-modal-active')
        first_output_details = $(this).data('d');
        ajaxHeaderDetails($(this).data('d'))
        ajaxHeaderCertainData($(this).data('d'))
        $(".ds-header").addClass('ds-header-active')
        $(".detailed-informations").addClass('detailed-informations-active')
        //lk Clearing the persistent check bug.
        console.log('Doing this here')
        warningDatesApproved = false;
        warningPricesApproved = false;
        warningQuantitiesApproved = false;
        $('.ds-approve-date span').removeClass('text-color-green')
        $('.approve-all .check-label-all').removeClass('label-check-mark')
        $('.approve-all span').removeClass('text-color-green')
        $('.ds-approve-date .check-label-dates').removeClass('label-check-mark')
        $('.ds-approve-date span').removeClass('text-color-green')
        //lk End
        var $this = $(this);
        var $selectedValue = $this.attr('data-value')
        if ($selectedValue) {
            $(".ds-title-first-data-dropdown span").html($selectedValue);
            $('.ds-first-data-dropdown').toggleClass("ds-toggle-dropdown");
        }
    });

    $('.ds-title-dropdown').on('click', function () {
        $('.ds-results-dropdown').toggleClass("ds-toggle-dropdown");
    });

    $('.ds-title-first-data-dropdown').on('click', function () {
        $('.ds-first-data-dropdown').toggleClass("ds-toggle-dropdown");
    });

    $(document).on('click', 'li', function () {
        $('li').removeClass('ds-active');
        $(this).addClass('ds-active');
    });  

    $('.ds-btn-submit').click(function(){
            $('.warning-modal-reject-buttons').css({"display":"none"})
            
            //get all fields
            
            let quantityInputs = $('.qty-input');
            let priceInputs = $('.price-input');
            let dateInputs = $('.ds-users-approved-date');
            let fields = [];
            let values = [];
            let datesValues = []
            let priceValues = []
            let quantityValues = []

            //get value from each field 
            quantityInputs.each(function(){
                fields.push($(this))
                values.push($(this).val());
                quantityValues.push($(this).val());
            })
            //get value from each field 
            priceInputs.each(function(){
                fields.push($(this))
                values.push($(this).val());
                priceValues.push($(this).val());
            })
            //get value from each field 
            dateInputs.each(function(){
                fields.push($(this))  
                values.push($(this).val());
                datesValues.push($(this).val());
            })
           
            //check if every field has a value
            datesValues.every(checkFields) ? $('.approve-delivery-days').addClass('warning-modal-buttons-disable') : $('.approve-delivery-days').removeClass('warning-modal-buttons-disable')
            quantityValues.every(checkFields) ? $('.approve-quantities').addClass('warning-modal-buttons-disable') : $('.approve-quantities').removeClass('warning-modal-buttons-disable')
            priceValues.every(checkFields) ? $('.approve-ask-prices').addClass('warning-modal-buttons-disable') : $('.approve-ask-prices').removeClass('warning-modal-buttons-disable')
           

            if(values.every(checkFields)) {
                
                $('.ds-btn-submit').attr('id', 'approved-submit');
                submit();
                
                setTimeout(function(){
                    $('.warning-modal').removeClass('warning-modal-active');
                    $('.main-warning-text').text('!תשובתך עודכנה בהצלחה').css({"color": "green","display": "block","font-weight": "bold"})
                },2000)
            }
            
            else {
                showInvalidInputs (fields)
            }    
    });

    $('.submit-button-reject').click(function(){
        $('.warning-modal-reject-buttons').css({"display":"flex"})
            $('.warning-modal').addClass('warning-modal-active');
            $('.main-warning-text').text('הזן סיבת דחייה')
            $('.main-warning-text').css({"display":"block", "color":"red"})
            $('.warning-modal-reject-buttons').addClass('warning-modal-buttons-active')
            
            $('.approve-delivery-days').addClass('warning-modal-buttons-disable')
            $('.approve-quantities').addClass('warning-modal-buttons-disable')
            $('.approve-ask-prices').addClass('warning-modal-buttons-disable')
            $('.approve-all-warning').addClass('warning-modal-buttons-disable')
    });

    $("#reject-yes").click(function(){
            $('.inner-buttons-container').css({"display":"none"})
            $('.reject-reason-input').css({"display":"none"})
            $('.main-warning-text').text('ההזמנה נדחתה. הודעה נשלחה למזמין').css({"color":"green"})
            
            setTimeout(function(){
                ajaxRejectData();
                $('.main-warning-text').css({"display":"none", "color":"green"})
                $('.approve-delivery-days').removeClass('warning-modal-buttons-disable')
                $('.approve-quantities').removeClass('warning-modal-buttons-disable')
                $('.approve-ask-prices').removeClass('warning-modal-buttons-disable')
                $('.approve-all-warning').removeClass('warning-modal-buttons-disable')
            },1500)
    });
    
    // CLICK EVENTS END

    $('#detailed-informations').on('change', '.ds-users-approved-date', function() {
        let input = $(this)
        if($(this).val() === "" || $(this).val() === " " || !$(this).val()) {
            input.removeClass('valid-input')
        }
   })

   // lk 

   $('#detailed-informations').on('change', '.ds-users-approved-date', function() {
        this.setAttribute(
            "newVal",
            moment(this.value, "YYYY/MM/DD")
            .format("")
        )
        console.log(this.getAttribute('newVal'))
    })

    //lk end




    $('#ds-form-details').on('change', '.price-input', function() {
        reformatInputs($(this), 3)
    });
    $(document).on('change', '.qty-input', function() {
        reformatInputs($(this), 2)
    });
    $('.reject-reason-input').on('keyup',function(){
        
        if($(this).val().length >= 5) {
            
            $('#reject-yes').removeClass('disabled-button')
        }

        else {
            $('#reject-yes').addClass('disabled-button')
        }
    });
    $(document).on("mouseenter", ".ds-suppliers-comments textarea", function() {
        let textValue = $(this).val()
        if(textValue !== '' && textValue.length > 2) {
            
            $(this).parents('.tbl-content').append(`<div class='textarea-display-text'><span>${textValue}</span></div>`)
            
            let offsetTop = $(this).offset().top - 20;
            let offsetLeft = $(this).offset().left + 55;
            
            $('.textarea-display-text').offset({ top: offsetTop, left: offsetLeft});
        } 
    }); 
    $(document).on("mouseleave", ".ds-suppliers-comments textarea", function() {
        $(this).parents('.tbl-content').find('.textarea-display-text').remove()
    });
    $("#ds-form-details").on('mouseenter', '.ds-item-description', function() {
        
        let text = $(this).text()
        if(text.length > 10) {
            $(this).parents('.tbl-content').append(`<div class='textarea-display-text'>${text}</div>`)
            let offsetTop = $(this).offset().top - 20;
            let offsetLeft = $(this).offset().left + 55;
            $('.textarea-display-text').offset({ top: offsetTop, left: offsetLeft});
        }
    });
    $("#ds-form-details").on('mouseleave', '.ds-item-description', function() {
        $(this).parents('.tbl-content').find('.textarea-display-text').remove()
    }); 
    $(document).on('change', '#approved', function () {
        if ($(this).is(':checked')) {
            $('.form-group .ds-not-approved-date').html("");

        };
    });
    $('#ds-search-products').keyup(delay(function (e) {
        $('#ds-section-search').find(".ds-title-dropdown span").html("פרויקט");
        $('.ds-first-data-dropdown').remove()
        $('.ds-title').css({'display':'none'})
        $(".ds-header").find("[data-name]").html('')
        $(".ds-container").find("[data-name]").html('')
        $(".ds-container").find("[data-name='Comments']").val('')
        $(".ds-wrap-2").css({"display":"none"});
        $(".ds-wrap-3").css({"display":"none"});
        $(".ds-project p").css({"display":"none"});
        $('.ds-project .ds-title').css({"display":"block"}).text('לא נמצאו נתונים')
        $('.ds-container-details').css({"display": "none"});
        $('.ds-container').css({'display': "none"});
        $(".detailed-informations").removeClass('detailed-informations-active')
        $(".ds-header").removeClass('ds-header-active')
        $('#ds-first-data').removeClass('ds-first-data-active')
        $('#ds-first-data').find(".ds-title-first-data-dropdown span").html("בחר הזמנה");
        

        searchField = $(this).val();
        if (searchField === '') {
            $('#filter-records').html('');
            $('#ds-section-search').find(".ds-title-dropdown span").html("Output...");

            $('#ds-filter-output').html("");
            return;
        }
        let $checkboxChange = $('#ds-chbox-bollean');
        if ($checkboxChange.is(":checked")) {
            $checkboxChange.val('true');
        } else {
            $checkboxChange.val('false');
        };
        checkbox = $checkboxChange.val();
        
        ajaxDetails()
    }, 50));
    $('#ds-first-data').find(".ds-title-first-data-dropdown span").html("בחר הזמנה");
    //lk Older version ajax call on change event
    $('.ds-container-header').on('change','.version-select', () => {
        console.log('Change event')
        ajaxCallOlderVersion(...headerData, $(this).find('option:selected').attr('value'))
    }) 
    
    var search = $(this).val();

    if (search === '') {
        $('#filter-records').html('');
        $('#ds-section-search').find(".ds-title-dropdown span").html("פרויקט");
        $('#ds-filter-output').html("");
        return;
    }
  

    //HELPER FUNCTIONS START
    function reformatInputs(input, digits) {
        console.log('input', input)
        let inputVal = $(input).val();
        let afterDot = inputVal.split('.').pop();
        console.log('input with $', $(input))
        console.log('input value', inputVal)

        if(inputVal.indexOf(".") != -1 && afterDot.length > 2) {
           
            $(input).val('')
            $(input).addClass('invalid-input')
            $(input).removeClass('valid-input')
            registerInputClick()
            console.log('length after dot is too high')
        }

        if(inputVal.indexOf(".") === -1 && digits === 2 ) {
            $(input).val(inputVal + '.00')
            console.log('added . to end', $(input).val())
        }

        if(inputVal.indexOf(".") === -1 && digits === 3 ) {
            $(input).val(inputVal + '.000')
            console.log('added . to end', $(input).val())
        }

        if(inputVal === '') {
            $(input).val('')
        }
    }
    function resetFields(fields) {
        
        for (var i = 0; i < fields.length; i++) {
            let field = fields[i]
            $(field).val('')
            $(field).removeClass('valid-input')
            $(field).addClass('invalid-input') 
        }
    };
    function showInvalidInputs (fields) {
        for (var i = 0; i < fields.length; i ++) {
            if(fields[i].val().length < 2) {
                fields[i].addClass('invalid-input')
            }
        }
        
        registerInputClick();

        $('.main-warning-text').css({"color": "red"})
        $('.warning-modal').addClass('warning-modal-active');
    };
    function checkFields(currentValue){
        return currentValue.length > 2
    };
    function delay(callback, ms) {
        var timer = 0;
        return function() {
          var context = this, args = arguments;
          clearTimeout(timer);
          timer = setTimeout(function () {
            callback.apply(context, args);
          }, ms || 0);
        };
    }; 
    function registerInputClick () {
        $('.tbl-content input').click(function(){
            if($(this).hasClass('invalid-input')) {
                $(this).removeClass('invalid-input')
            }
        })
    };

    function copyDatesToFields (button) {
        let dates = $('.date-data')
        let dateFields = $('.ds-users-approved-date')
        let text;
        
        for (var i = 0; i < dates.length; i++) {

            let date = dates[i]
            let field = dateFields[i]

            if(button) {
                text = button.parent().find('span');
            }
            let confDate = new Date($(date).data('date'))
            
            let confText = reFormatDate(confDate)


           
            // this if places value into input if it has no value
            if($(field).val() === "" || !$(field).val()) {
              
                if(button) {
                    button.addClass('label-check-mark')
                    text.addClass('text-color-green')
                    text.text('תאריכים אושרו')
                }
                
                $(field).removeClass('invalid-input')
                $(field).addClass('valid-input')
                $(field).val(confText)
                $(field).data( "changed", true );

                $('.approve-delivery-days .checkmark-first-part').addClass('checkmark-active')
                
                warningDatesApproved = true
                
            }
            //this if removes value if value is same as text value
            else if($(field).val() !=="" && $(field).val() === confText && $(field).data( "changed")) {
                
                $(field).val('') 
                $(field).removeClass('valid-input')
                warningDatesApproved = false
                
                $('.approve-delivery-days .checkmark-first-part').removeClass('checkmark-active')

                if(button) {
                    button.removeClass('label-check-mark')
                    text.removeClass('text-color-green')
                    text.text('אשר תאריכים ')
                }  
            } 
        }
    };
    function copyQuantityToFields () {
        let quantities = $('.quantity-data')
        let quanititesFields = $('.qty-input')
        

        for (var i = 0; i < quantities.length; i++) {
            let quantity = quantities[i]
            let field = quanititesFields[i]
            

            if($(field).val() === "" || !$(field).val()) {

                let newVal = reFormatNumbers($(quantity).text())

                $(field).val(newVal)
                $(field).removeClass('invalid-input')
                $(field).addClass('valid-input')
                
                $('.approve-quantities .checkmark-first-part').addClass('checkmark-active')
                
                warningQuantitiesApproved = true
            }
            
            else if($(field).val() !== "" && $(field).val() === $(quantity).text()) {
                $(field).val('') 
                $(field).removeClass('invalid-input')
                $(field).removeClass('valid-input')

                $('.approve-quantities .checkmark-first-part').removeClass('checkmark-active')

                warningQuantitiesApproved = false
            }
        } 
    };
    function copyPriceToFields () {
        let prices = $('.price-data')
        let pricesFields = $('.price-input')
        

        for (var i = 0; i < prices.length; i++) {
            let price = prices[i]
            let field = pricesFields[i]
            let checker = $(price).text().replace('₪ ','').replace('€ ','');

            if($(field).val() === "" || !$(field).val()) {
                
                let newVal = reFormatNumbers($(price).text())

                $(field).val(newVal)
                $(field).removeClass('invalid-input')
                $(field).addClass('valid-input')
                
                $('.approve-ask-prices .checkmark-first-part').addClass('checkmark-active')
                
                warningPricesApproved = true
            }
            

            else if($(field).val() !=="" && $(field).val() === checker) {
                $(field).val('') 
                $(field).removeClass('invalid-input')
                $(field).removeClass('valid-input')

                $('.approve-ask-prices .checkmark-first-part').removeClass('checkmark-active')

                warningPricesApproved = false
            }
        } 
    };
    function disableButtons () {
        let dropdown = dropdown_details;
        let rows = $('.info-container-row');
        
        for(var i = 0; i < rows.length; i ++) {
            
            if($(rows[i]).hasClass('disabled-row')) {
                
                $('.ds-last-part').addClass('last-part-disabled')
                $('.ds-btn-box').css({"pointer-events": "none", "opacity": "0.6"})
                $('.disabled-row input').css({"transition": "all 0.3s", "pointer-events" : "none", "background": "#D0D0D0"});
                $('.disabled-row textarea').css({"transition": "all 0.3s","pointer-events" : "none", "background": "#D0D0D0"});
                $('.last-part-disabled textarea').css({"transition": "all 0.3s","pointer-events" : "none", "background": "#D0D0D0"});
            }

            else if(!$(rows[i]).hasClass('disabled-row')){
                
                $('.last-part-disabled textarea').css({"transition": "all 0.3s","pointer-events" : "all", "background": "white"});
                $('.ds-btn-box').css({"pointer-events": "all", "opacity": "1"})
                $('.ds-last-part').removeClass('last-part-disabled') 
            }
        }
    };
    function submit() {
        //headerData and certainData are global variables that are created when ajax returns data
        // this function is looping trough data and creating new key-value pair if needed or updating old key-value

        let firstData = $('[data-name]')
        let secondData = $('.info-container-row')

        //ITTERATION FOR FIRST DATA
        
        //loop trough object
        for (var i = 0; i< firstData.length; i ++){
            
            let item = $(firstData[i])
            let itemData = item.data('name')
            let singleObject = headerData[0]

            if(item.is('input') && item.val().length > 1) {
                //check if key by that name already exists
                if(singleObject.itemData) {
                    
                    singleObject.itemData = item.val()
                }
                //if it doesn't exist,create new key-value
                else {
                    
                    singleObject[itemData] = item.val()
                    
                }
            }

            else if (item.is('textarea') && item.val().length > 1) {
                //check if key by that name already exists
                if(singleObject.itemData) {
                    
                    singleObject.itemData = item.val()
                }
                //if it doesn't exist,create new key-value
                else {

                    singleObject[itemData] = item.val()
                   
                }
                
            }
            else {
                //check if key by that name already exists
                if(singleObject.itemData) {
                    
                    singleObject.itemData = item.text()
                }
                //if it doesn't exist,create new key-value
                else {

                    singleObject[itemData] = item.text()    
                }
            }
        }
        
        
        //ITTERATION FOR SECOND DATA
        
        //loop trough array for each object
        for (var k = 0; k< certainData.length; k++) {
            let row = secondData[k]
            let rowData = $(row).find('[data-multiple-name]') 
            let multiObject = certainData[k]
            
            //loop trough each object from array
            
            for (var i = 0; i< rowData.length; i ++){
                
                let item = $(rowData[i])
                let itemData = item.data('multiple-name')
    
                if(item.is('input') && item.val().length > 1) {
                    //check if key by that name already exists
                    if(multiObject.itemData) {
                        
                        multiObject.itemData = item.val()
                    }
                    //if it doesn't exist,create new key-value
                    else {
                        
                        multiObject[itemData] = item.val()
                        
                    }
                }
    
                else if (item.is('textarea') && item.val().length > 1) {
                    //check if key by that name already exists
                    if(multiObject.itemData) {

                        multiObject.itemData = item.val()
                    }
                    //if it doesn't exist,create new key-value
                    else {
                        multiObject[itemData] = item.val()
                    }
                    
                }
                 else {
                     //check if key by that name already exists
                     if(multiObject.itemData) {

                        multiObject.itemData = item.text()
                    }
                    //if it doesn't exist,create new key-value
                    else {
                        
                        multiObject[itemData] = item.text()
                    }
                 }
            }
        }

        //AJAX SUBMIT CALL

        ajaxSubmitData(certainData,headerData)
    };
    // lk format onece more
    function reReFormatDate(d){
        let date = d.split('/')
        return date[2] + '/' + date[1] + '/' + date[0]
    }
    function shipDateFormat(d){
        let date = d.split('-')
        return '20' + date[2] + '-' + date[1] + '-' + date[0]
    }
    function formatDate(d) {  

    var month = d.getMonth();
    var day = d.getDate().toString();
    var year = d.getFullYear();
    
    year = year.toString().substr(-2); 
    month = (month + 1).toString();
    
    if (month.length === 1) {
        month = "0" + month;
    }

    if (day.length === 1) {
        day = "0" + day;
    }
    //lk
    //return day + '/' + month + '/' + year
    return year + "/" +  month + "/" + day;
    };
    function reFormatDate(d) {
        var month = d.getMonth();
        var day = d.getDate().toString();
        var year = d.getFullYear();
    
        year = year.toString(); 
        month = (month + 1).toString();
    
        if (month.length === 1) {
            month = "0" + month;
        }

        if (day.length === 1) {
            day = "0" + day;
        }

        return year + "-" +  month + "-" + day;
    };
    function formatNumbers(num) {
        let char = ","
        let number = num.toString()
        let position 
        let position2

        if(number.length <= 1 || !number) {
            return ''
        }

        if(number.length <= 7) {
            
            return number
        }

        if(number.length <= 8) {
            position = 1
           
            return number.substring(0, position) + char + number.substring(position, number.length);    
        }

        if(number.length <= 9) {
            position = 2
            
            return number.substring(0, position) + char + number.substring(position, number.length);
        }

        if(number.length <= 10) {
            position = 1
            position2 = 4
            return number.substring(0, position) + char + number.substring(position, position2) + char + number.substring(position2, number.length);
        }
        
    };
    function reFormatNumbers(num) {  
        // let conf
        // if(fixed === 2) {
        //     conf = num.substring(0, num.length - 2);
        // }

        // if(fixed === 3) {
        //     conf = num.substring(0, num.length - 3)
        // }
        // let number = conf.replace(/\./, '')
        
        // let changed = Number(num)

        return num.replace('₪ ','').replace('€ ','')
        
    };
    function reFormatNumbersForDB(num) {
        
        let conf = num.replace('₪ ','').replace('€ ','').replace(/ /g,'')

        if(conf.length > 7) {
            conf = conf.replace(',','')
            return Number(conf).toFixed(6)
        }
        
        return Number(conf).toFixed(6)
         
    };
    // lk Setting the version picker initially
    function handleVersionInit({version, DocEntry}){
        // Setting the options based on the number of versions.
        if(version != 1){
            $(".version-select").empty()
            for(i = 0; i < version; i++){
                $(".version-select").append($('<option>',{
                    value: i,
                    text: `${i} גרסא`
                }))
            }
        }else if(version == 1) {
            console.log('Intoversion 1')
            $('.version-select').empty().append($('<option>',{
                value: 0,
                text: `0 גרסא`
            })).attr('disabled', '')
        }


        console.log('version: ' + version)
        console.log('docentry: ' + DocEntry)
    }


    
    //HEPLER FUNCTIONS END    


    // AJAX CALLS 
    function ajaxDetails () {
        
        $.ajax({
            type: "POST",
            url: "/data",
            data: {
                checkbox: checkbox,
                searchField: searchField,
                value: dropdown_details
            },
            success: function (data) {
                const confData = data
                
                const uniqueData = Array.from(new Set(confData.map(a => a.project_code)))
                    .map(project_code => {
                        return confData.find(a => a.project_code === project_code)
                    })
                   
              
                try {
                    
                    var regex = new RegExp(searchField, "i");
                    var output = '<ul class="ds-results-dropdown">';
                    var count = 1;
                    
                    $.each(uniqueData, function (key, val) {

                        if ((val.project_name.search(regex) != -1) || (val.project_code.search(regex) != -1) || (val.doc_number.search(regex) != -1)) {
                            output += `<li class="ds-results" data-value="${val.project_name} ${val.project_code}" data-branko="${val.project_code}"> ${val.project_name} ${val.project_code}</li>`;
                            count++;
                        }

                    });
                    output += '</ul>';
                    $('#ds-filter-output').html(output);

                   

                } catch (e) {
                    console.log('ajax details error',e);
                }
            },
            error: function(error) {
                console.log('search bar ajax error', error)
            } 

    });
    };
    function ajaxAdditionalDetails (data,dropdown) {
       
        $.ajax({
            type: "POST",
            url: "/aditionalDetails",
            data: { 'id':data, 'value': dropdown},
            success: function (data) {

                if (data === undefined || data.length == 0) {

                    $('.ds-title-first-data-dropdown span').text('לא נמצאו הזמנות')
                    $('.ds-header').removeClass('ds-header-active')
                    $(".ds-header").find("[data-name]").html('')
                    $(".ds-container").find("[data-name]").html('')
                    $(".ds-container").find("[data-name='Comments']").val('')
                    $(".ds-wrap-2").css({"display":"none"});
                    $(".ds-wrap-3").css({"display":"none"});
                    $(".ds-project p").css({"display":"none"});
                    $('.ds-project .ds-title').css({"display":"block"}).text('לא נמצאו נתונים')
                    $('.ds-container-details').css({"display": "none"});
                    $('.ds-container').css({'display': "none"});
                }

                else if(data.length > 0) {
                    $('.ds-title-first-data-dropdown span').text('בחר הזמנה')
                }
                let $firstData = '<ul class="ds-first-data-dropdown"><li class="first-data-dropdown-header"><p>סטאטוס</p><p>גרסא</p><p>סכום לפני מע"מ</p><p>הזמנה</p><p>תאריך</p></li>';
                
                for (let i in data) {
                    let status = data[i].supplier ? 'סגור' :  'פתוח'
                    let confdate = data[i].DocDate.split(' ')[0]; 
                    let date = formatDate(new Date(confdate))
                    let version = data[i].U_POVersion === '' ? '0' : data[i].U_POVersion
                    let confbeforeVat = Number(data[i].SumBeforVat).toFixed(2);    
                    
                    //format numbers was here
                    let beforeVat = formatNumbers(confbeforeVat)

                    $firstData += ` <li class="ds-first-output first-output-headered" data-value="${data[i].CardCode}&nbsp;  &nbsp;${data[i].CardName} &nbsp;  &nbsp;${date}" data-d="${data[i].DocEntry}">
                            <p>${status}</p>
                            <p>${version}</p>
                            <p>${beforeVat}</p>
                            <p>${data[i].DocNum}</p>
                            <p>${date}</p>
                    </li>`;
                }
                
                $firstData += '</ul>'
                
                $('#ds-first-filter-output').html($firstData);
            
            },
            error: function (error) {
               
                console.log('ajax additional details error',error)
            }
        })
    };
    function ajaxHeaderDetails (data) {
        
        let dropdown = dropdown_details
    
        $.ajax({
            type: "POST",
            url: "/headerDetails",
            data: { 'id': data, 'value': dropdown },
            success: function (data) {
                console.log('header data', data)
                headerData = data

                let confDueDate = data[0].DocDueDate ? new Date(data[0].DocDueDate) : '';
                let confNewDate = data[0].DocDate ? new Date(data[0].DocDate) : '';
                //lk reReFormatDate() again
                let DueDate = reReFormatDate(formatDate(confDueDate))
                let NewDate = reReFormatDate(formatDate(confNewDate))
                
                //2 digits after .
                let beforeVat = Number(data[0].SumBeforVat).toFixed(2);
                let vatSum = Number(data[0].VatSum).toFixed(2);
                let docTotal = Number(data[0].DocTotal).toFixed(2);
                
                //format numbers was here
                let toFixedbeforeVat = formatNumbers(beforeVat)
                let toFixedVatSum = formatNumbers(vatSum)
                let toFixedDocTotal = formatNumbers(docTotal)
                            

                try {
                    
                    $(".ds-header").find("[data-name]").html('')
                    $(".ds-container").find("[data-name]").html('')
                    $(".ds-container").find("[data-name='Comments']").val('')

                    $(".ds-header").find("[data-name='PrjName']").html('<span class="name-data-item">' + data[0].PrjName === null ? '' : data[0].PrjName + '</span>')
                    $(".ds-header").find("[data-name='Project']").html('<span class="name-data-item">' + data[0].Project === null ? '' : data[0].Project + '</span>')
                    $(".ds-header").find("[data-name='CardCode']").html('<span class="name-data-item">' + data[0].CardCode === null ? '' :  data[0].CardCode + '</span>')
                    $(".ds-header").find("[data-name='CardName']").html('<span class="name-data-item">' + data[0].CardName === null ? '' : data[0].CardName + '</span>')
                    $(".ds-header").find("[data-name='SuppliersContact']").html('<span class="name-data-item">' + data[0].SuppliersContact === null ? '' :  data[0].SuppliersContact + '</span>')
                    $(".ds-header").find("[data-name='DocDueDate']").html('<span class="name-data-item">' + data[0].DocDueDate === null ? '' : DueDate + '</span>')
                    $(".ds-header").find("[data-name='DocDate']").html('<span class="name-data-item">' + data[0].DocDate === null ? '' : NewDate + '</span>')
                    $(".ds-header").find("[data-name='DocNum']").html('<span class="name-data-item">' + data[0].DocNum === null ? '' : data[0].DocNum + '</span>')
                    $(".ds-header").find("[data-name='Address']").html('<span class="name-data-item">' + data[0].Address === null ? '' : data[0].Address + '</span>')
                    $(".ds-header").find("[data-name='DocCur']").html('<span class="name-data-item">' + data[0].DocCur === null ? '' : data[0].DocCur + '</span>')
                    $(".ds-header").find("[data-name='DocTotalFC']").html('<span class="name-data-item">' + data[0].DocTotalFC === null ? '' :data[0].DocTotalFC + '</span>')
                    $(".ds-header").find("[data-name='JrnlMemo']").html('<span class="name-data-item">' + data[0].JrnlMemo === null ? '' : data[0].JrnlMemo + '</span>')
                    $(".ds-header").find("[data-name='DocRate']").html('<span class="name-data-item">' + data[0].DocRate === null ? '' : data[0].DocRate + '</span>')
                    $(".ds-header").find("[data-name='DocEntry']").html('<span class="name-data-item">' + data[0].DocEntry === null ? '' : data[0].DocEntry + '</span>')
                    $(".ds-header").find("[data-name='OPVersion']").html('<span class="name-data-item">' + data[0].OPVersion === null ? '' :  data[0].OPVersion + '</span>')
                    $(".ds-header").find("[data-name='PaymentTerms']").html('<span class="name-data-item">' + data[0].PaymentTerms === null ? '' : data[0].PaymentTerms + '</span>')
                    $(".ds-header").find("[data-name='SumBeforVat']").html('<span class="name-data-item">' + data[0].SumBeforVat === null ? '' : toFixedbeforeVat + '</span>')
                    $(".ds-header").find("[data-name='VatSum']").html('<span class="name-data-item">' + data[0].VatSum === null ? '' : toFixedVatSum + '</span>')
                    $(".ds-header").find("[data-name='DocTotal']").html('<span class="name-data-item">' + data[0].DocTotal === null ? '' : toFixedDocTotal + '</span>')
                    $(".ds-header").find("[data-name='OPVersion']").html('<span class="name-data-item">' + data[0].OPVersion === null ? '' : data[0].OPVersion + '</span>')
                    $(".ds-header").find("[data-name='POVersion']").html('<span class="name-data-item">' + data[0].POVersion === null ? '' : data[0].POVersion + '</span>')
                    $(".ds-container").find("[data-name='SumBeforVat']").html('<span class="name-data-item">' + data[0].SumBeforVat === null ? '' : toFixedbeforeVat + '</span>')
                    $(".ds-container").find("[data-name='VatSum']").html('<span class="name-data-item">' + data[0].VatSum === null ? '' : toFixedVatSum + '</span>')
                    $(".ds-container").find("[data-name='PaymentTerms']").html('<span class="name-data-item">' + data[0].PaymentTerms === null ? '' : data[0].PaymentTerms + '</span>')
                    $(".ds-container").find("[data-name='DocTotal']").html('<span class="name-data-item">' + data[0].DocTotal === null ? '' : toFixedDocTotal + '</span>')
                    $(".ds-container").find("[data-name='Comments']").val( data[0].Comments === null ? '' : data[0].Comments )
                    //lk
                    handleVersionInit(...data)
                } catch (e) {

                    console.log('ajax header details error', e)
                }
            }
        })
    };
    function ajaxHeaderCertainData (data) {
        
        let dropdown = dropdown_details
        
        $.ajax({
            type: "POST",
            url: "/certenData",
            data: { 'id': data, 'value': dropdown },
            success: function (data) {
                
                certainData = data
                console.log('certain data', data)
                
                    try {

                    let n = 1;
                    let output;

                    if (data === undefined || data.length === 0) {
                        output += `<tr class='info-container-row'>
                        </tr>`

                        $('.tbl-content table tbody').html(output)
                        $(".ds-header").find("[data-name]").html('')
                        $(".ds-container").find("[data-name]").html('')
                        $(".ds-container").find("[data-name='Comments']").val('')
                        $(".ds-wrap-2").css({"display":"none"});
                        $(".ds-wrap-3").css({"display":"none"});
                        $(".ds-project p").css({"display":"none"});
                        $('.ds-project .ds-title').css({"display":"block"}).text('לא נמצאו נתונים')
                        $('.ds-container-details').css({"display": "none"});
                        $('.ds-container').css({'display': "none"});
                    }
                    
                    else {
                        $(".ds-wrap-2").css({"display":"block"});
                        $(".ds-wrap-3").css({"display":"block"});
                        $(".ds-project p").css({"display":"inline-block"});
                        $('.ds-project .ds-title').css({"display":"block"}).text('פרויקט')
                        $('.ds-container-details').css({"display": "block"});
                        $('.ds-container').css({'display': "flex"});
                        
                        for (let i in data) {
                            
                            let confCurrency = data[i].Currency ? data[i].Currency + ' ' : '';
                            let currency = confCurrency === 'Eur ' ? '€ ' : confCurrency;
                            let confshipdate = data[i].ShipDate
                            let confSuppPrice = data[i].U_SuppPrice ? data[i].U_SuppPrice : '';
                            let shipdate = confshipdate.replace(/-/g, '/')
                            //lk display dd/mm/yy reReFormateDate() samo
                            let newDate = reReFormatDate(formatDate(new Date(shipdate)))
                            let date = formatDate(new Date(shipdate))
                            console.log('date', date)
                            console.log('newDate', newDate)
                            
                            let confApprovedDate = data[i].U_ApprovedSupplyDate ? data[i].U_ApprovedSupplyDate.split(' ')[0] : '';
                            let approvedDate = confApprovedDate.replace(/-/g, '/')
                            let rowClass = data[i].U_Supplier !== null ? 'disabled-row' : ''; 
                            let remarks = data[i].U_SuppFreeText ? data[i].U_SuppFreeText : 'אין טקסט';
                            let lineComments = data[i].LineComments === " "  ? 'אין תגובה' : data[i].LineComments;
                            let confSuppQuantity = data[i].U_SuppQuantity ? data[i].U_SuppQuantity : '';
                            //2 digits after .
                            let quantity = Number(data[i].Quantity).toFixed(2);
                            let discPrcnt = Number(data[i].DiscPrcnt).toFixed(2);
                            let lineTotal = Number(data[i].LineTotal).toFixed(2);
                            let secondConfsuppQuantity = confSuppQuantity ? Number(confSuppQuantity).toFixed(2) : ''

                            // format numbers was here
                            let toFixedQuantity = formatNumbers(quantity)
                            let toFixedDiscPrcnt = formatNumbers(discPrcnt)
                            let toFixedlineTotal = formatNumbers(lineTotal) 
                            let suppQuantity = formatNumbers(secondConfsuppQuantity)
                            
                            //3 digits after .
                            let price = Number(data[i].Price).toFixed(3);
                            let priceBeforeDi = Number(data[i].PriceBefDi).toFixed(3);
                            let secondConfsuppPrice =  confSuppPrice ? Number(confSuppPrice).toFixed(3) : '';
                            
                            //format numbers was here
                            let toFixedPrice = formatNumbers(price)
                            let toFixedpriceBeforeDi = formatNumbers(priceBeforeDi)
                            let suppPrice = formatNumbers(secondConfsuppPrice)
                            
                            
                            output += ` <tr data-id=${n + 'dataid'} class='info-container-row ${rowClass}'> 
                                    <td class="ds-suppliers-comments textarea-container"> 
                                        <textarea value="${lineComments}" data-multiple-name="LineComments" id="" placeholder="הערות ספק לשורה">${lineComments}</textarea> 
                                    </td>
                                    <td class="ds-total-line" data-multiple-name="LineTotal">${ currency + toFixedlineTotal}</td>
                                    <td class="ds-supp-price"> 
                                    <input class="price-input" type="text" value="${suppPrice}" data-multiple-name="U_SuppPrice" />
                                    </td>
                                    <td class="ds-after-discount" data-multiple-name="Price">${ currency + toFixedPrice}</td>
                                    <td class="ds-discount" data-multiple-name="DiscPrcnt">${toFixedDiscPrcnt}</td>
                                    <td class="ds-price">
                                        <span class="price-data"  data-multiple-name="PriceBefDi">${ currency + toFixedpriceBeforeDi}</span>
                                    </td>
                                    <td class="ds-supp-qty"> 
                                        <input class="qty-input" data-multiple-name="U_SuppQuantity" type="text" value="${suppQuantity}" />
                                    </td>
                                    <td class="da-qty" >
                                        <span data-multiple-name="Quantity" class="quantity-data">${toFixedQuantity}</span>
                                    </td>
                                    <td class="ds-remarks" >
                                        <input type="text" data-multiple-name="U_SuppFreeText" disabled value="${remarks}">
                                    </td>
                                    <td class="ds-approveddate">
                                        <input type='date' value="${confApprovedDate}" data-multiple-name="U_ApprovedSupplyDate" class="ds-users-approved-date meeting"  />
                                    </td>
                                    <td class="ds-shipdate">
                                        <span class="date-data" data-date=${confshipdate} data-multiple-name="ShipDate">${newDate}</span>
                                    </td>
                                    <td class="ds-po-version">
                                        <input type="number" value="${data[i].SubCatNum}" data-multiple-name="SubbCatNum" />
                                    </td>
                                    <td class="ds-po-version">
                                        <input type="number" value="${data[i].VendorNum}" data-multiple-name="VendorNum" />
                                    </td>
                                
                        
                                    <td class="ds-item-description" data-multiple-name="Dscription">${data[i].Dscription}</td>
                                    <td class="ds-item-code" data-multiple-name="ItemCode">${data[i].ItemCode}</td>
                                    <td class="ds-serial-number" data-multiple-name="SerialNumber">${n++}</td>
                                    <td class="hidden-fields" data-multiple-name="LineDocEntry">${data[i].LineDocEntry}</td>
                                    <td class="hidden-fields" data-multiple-name="LineNum">${data[i].LineNum}</td>
                                    <td class="hidden-fields" data-multiple-name="VisOrder">${data[i].VisOrder}</td>
                                </tr>
                                `
                        }

                        //inputs and buttons disable initiation
                        setTimeout(function(){
                            let dates = $('.ds-users-approved-date')
                            for(var i = 0; i < dates.length; i++) {

                                if(dates[i].value === '1900-01-01') {
                                    dates[i].value = ''
                                }
                            }
                            disableButtons ()
                        },100)
                        

                        $('.tbl-content table tbody').html(output)    
                        
                    } 
                    
            } catch (e) {
                console.log('ajax certain data error', e)
            }
        },
    })

    };
    function ajaxSubmitData(multipleObj,uniqeObj) {
        let multiple = multipleObj;
        let single = uniqeObj;

        // reconfiguring data for backend 

        //singleObject
        single[0].DocDate = single[0].DocDate.replace(/[/]/g,'-')
        single[0].DocDueDate = single[0].DocDueDate.replace(/[/]/g,'-')
        single[0].SumBeforVat = reFormatNumbersForDB(single[0].SumBeforVat)
        single[0].VatSum = reFormatNumbersForDB(single[0].VatSum)
        single[0].DocNum = single[0].DocNum
        single[0].DocEntry = single[0].DocEntry
        single[0].DocTotal = reFormatNumbersForDB(single[0].DocTotal)
        //multipleObjects
        multiple.forEach(function (arrayItem) {
            arrayItem.DiscPrcnt = arrayItem.DiscPrcnt
            arrayItem.Price = reFormatNumbersForDB( arrayItem.Price )
            arrayItem.U_SuppQuantity = reFormatNumbersForDB(arrayItem.U_SuppQuantity )
            arrayItem.U_SuppPrice = reFormatNumbersForDB(arrayItem.U_SuppPrice )
            arrayItem.Quantity = reFormatNumbersForDB(arrayItem.Quantity )
            arrayItem.PriceBefDi =  reFormatNumbersForDB( arrayItem.PriceBefDi )
            //lk shipDate format
            arrayItem.ShipDate = shipDateFormat(arrayItem.ShipDate.replace(/[/]/g,'-'))
            console.log('ShipDate: ', arrayItem.ShipDate)
            arrayItem.U_ApprovedSupplyDate = arrayItem.U_ApprovedSupplyDate.replace(/[/]/g,'-')
            arrayItem.LineTotal = reFormatNumbersForDB( arrayItem.LineTotal )
            arrayItem.LineDocEntry = arrayItem.LineDocEntry
            arrayItem.DiscPrcnt = arrayItem.DiscPrcnt
        });

        console.log('final data checking for changes', 'single object', single, 'multiple array', multiple)

        $.ajax({
            type: "POST",
            url: "/submit",
            data: {
                'multipleArr': multiple,
                'wholeOb': single
            },
            success: function () {
                
                $('.warning-modal').addClass('warning-modal-active');
                $('.main-warning-text').text('!תשובתך עודכנה בהצלחה').css({"color": "green","display": "block","font-weight": "bold"})
                $('.approve-delivery-days').css({"display": "none"})
                $('.approve-quantities').css({"display": "none"})
                $('.approve-ask-prices').css({"display": "none"})
                $('.approve-all-warning').css({"display": "none"})
                $('.info-container-row').addClass('disabled-row')
                $('.ds-last-part').addClass('last-part-disabled')
                $('.ds-btn-box').css({"pointer-events": "none", "opacity": "0.6"})
                $('.disabled-row input').css({"transition": "all 0.3s", "pointer-events" : "none", "background": "#D0D0D0"});
                $('.disabled-row textarea').css({"transition": "all 0.3s","pointer-events" : "none", "background": "#D0D0D0"});
                $('.last-part-disabled textarea').css({"transition": "all 0.3s","pointer-events" : "none", "background": "#D0D0D0"});
            
            },
            error: function (e) {
                
                $('.warning-modal').addClass('warning-modal-active');
                $('.main-warning-text').text('error submiting data').css({"color": "red","display": "block","font-weight": "bold"})
                $('.approve-delivery-days').css({"display": "none"})
                $('.approve-quantities').css({"display": "none"})
                $('.approve-ask-prices').css({"display": "none"})
                $('.approve-all-warning').css({"display": "none"})
                
                console.log('ajax submit error',e);
            }
    
    
         })
    };
    function ajaxRejectData(){
        let rejectReason = $('.reject-reason-input').val()
                $('.approve-delivery-days').css({"display": "none"})
                $('.approve-quantities').css({"display": "none"})
                $('.approve-ask-prices').css({"display": "none"})
                $('.approve-all-warning').css({"display": "none"})
                $('.info-container-row').addClass('disabled-row')
                $('.ds-last-part').addClass('last-part-disabled')
                $('.ds-btn-box').css({"pointer-events": "none", "opacity": "0.6"})
                $('.disabled-row input').css({"transition": "all 0.3s", "pointer-events" : "none", "background": "#D0D0D0"});
                $('.disabled-row textarea').css({"transition": "all 0.3s","pointer-events" : "none", "background": "#D0D0D0"});
                $('.last-part-disabled textarea').css({"transition": "all 0.3s","pointer-events" : "none", "background": "#D0D0D0"});
        $.ajax({
            type: "POST",
            url: "/reject",
            data: {
                'DocEntry': first_output_details,
                'wholeOb': headerData,
                'U_Reject_Reason': rejectReason
            },
            success: function () {
                
                $('.warning-modal').removeClass('warning-modal-active')
            },
            error: function (e) {
                console.log('ajax reject error',e);
                location.reload()
            }
    
    
        })
    };
    //lk ajax older version
    function ajaxCallOlderVersion({DocEntry}, version){
        $.ajax({
            method: 'post',
            url: '/version',
            data: {
                'U_POVersion': version,
                'DocEntry': DocEntry
            },
        }).done((data)=>{
                console.log('Data spec vers: ', data)
        }).fail((e)=>console.log('An error has occured!', e))
        console.log('AjaxOlderVersionFn')
        console.log(DocEntry)
        console.log(version)
    }
});